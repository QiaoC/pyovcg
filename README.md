# Simplified PyOVCG

## Introduction

This is a python interface for *Overture/CG* solvers. For more information about Overture and CG (composite grids), please take a look at the [official website](http://www.overtureframework.org/). Note that the software on LLNL may be broken, the current under-developing version can be found [here](https://sourceforge.net/projects/overtureframework/?source=directory). To build Overture/CG with current Ubuntu environment, [this repository](https://github.com/unifem/overture-desktop) may be helpful.

## Limitations

Note that this is not a 1-1 wrapper for Overture/CG (this is a potential project anyway). For each solver in CG, there is an intermediate C++ interface to ease the further wrapping process with Cython. **Also, please be aware that only serial version is considered.** Finally, please be aware that this is a mission-driven project, and only tested for Ubuntu build with [this docker image](https://github.com/unifem/coupler-desktop), so it probably cannot fit in your needs.

## Installation and Requirements

In the docker image (mentioned above), navigate to this directory and then invoke ```sudo python3 setup.py install```.

Currently, this software has the following Requirements for installation:

* A complete Overture/CG installation based on previous git repository

* Python3.5 or higher

* NumPy 1.11 or higher

* A C++ compiler

Optionally, if you want to regenerate all C++ source files, you need to have Cython installed and excute ```./gen_cython -f```.

## Solvers

* Overture multiphysics solver ```cgmp```: working, testing purpose

* Overture incompressible Navier-Stokes w/ buoyancy source term ```cgins```: the solver is working, interface is still under developing.

## Jupyter Notebooks

This software comes with several jupyter notebooks as tutorials. Inside ```./notebooks``` directory, copy the examples into your prefered location and enjoy. For instance, ```cp -r notebooks/cgmp/multiDomain $HOME/```.

## License
Copyright (c) 2018 Qiao Chen - All rights reserved.

The **pyovcg** library is licensed under the New (Revised) BSD license. Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the **pyovcg** nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Contact
Qiao Chen, benechiao@gmail.com
