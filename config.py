import os
import numpy
from distutils.extension import Extension

pkgs = []
_abs_path = os.path.dirname(os.path.abspath(__file__))
for pkg in os.listdir(_abs_path):
    if pkg.endswith('_pkg.py'):
        pkgs.append(__import__(pkg.split('.')[0]))
ext = []  # global

_home = os.environ.get('HOME', default=None)
_home = _home if _home else os.environ.get('DOCKER_HOME', default=None)
if _home is None:
    raise EnvironmentError('$HOME or $DOCKER_HOME must be setup!')

_ov_dir = os.environ.get('Overture', default=None)
_ov_dir = _ov_dir if _ov_dir else _home + '/overture/Overture.bin'
_cg_dir = os.environ.get('CGBUILDPREFIX', default=None)
_cg_dir = _cg_dir if _cg_dir else _home + '/overture/cg.bin'
_solvers = ['/ad', '/asf', '/ins', '/cns', '/sm', '/mp']  # NOTE no mx
_cg_dirs = [_cg_dir + sol for sol in _solvers]
_app_dir = os.environ.get('APlusPlus', None)
_app_dir = \
    _app_dir if _app_dir else _home + '/A++P++.bin/A++/install'
_cg_src_dir = os.environ.get('CG', None)
_cg_src_dir = _cg_src_dir if _cg_src_dir else _home + '/overture/cg'
_cg_src_dirs = [_cg_src_dir + sol for sol in _solvers]
_libs = ['Overture', 'Ogmg', 'Rapsodi', 'CgCommon', 'App']
for sol in _solvers:
    _libs.append('Cg' + sol.split('/')[1])
_libs.append('stdc++')
_cg_common = _cg_dir + '/common'
_cg_src_common = _cg_src_dir + '/common'
_lib_dirs = ['/usr/lib/x86_64-linux-gnu',
             _ov_dir + '/lib',
             _cg_common + '/lib',
             _app_dir + '/lib']
_inc_dirs = ['.',
             _abs_path,
             _ov_dir + '/include',
             _cg_src_common + '/src',
             _app_dir + '/include',
             _cg_src_common + '/chemistry',
             _cg_src_common + '/moving/src',
             _cg_src_common + '/multiComponent/src',
             numpy.get_include()]
for sol in _cg_dirs:
    _lib_dirs.append(sol + '/lib')
for sol in _cg_src_dirs:
    _inc_dirs.append(sol + '/src')


def gen_ext(name, srcs):
    global ext
    ext.append(Extension(
        name,
        srcs,
        include_dirs=_inc_dirs,
        library_dirs=_lib_dirs,
        libraries=_libs,
        runtime_library_dirs=_lib_dirs,
        extra_compile_args=['-w', '-O2', '-march=native'],
        language='c++'))


for pkg in pkgs:
    for i in range(pkg.count):
        gen_ext(getattr(pkg, 'name%i' % i), getattr(pkg, 'srcs%i' % i))
