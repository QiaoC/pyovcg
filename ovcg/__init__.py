"""OVCG init file"""

import ovcg._config
from .common import *
from .cgmp import *
from .cgins import *
from .io import *
from ._version import __version__

ov_dir, cg_dir = ovcg._config._ov_dir, ovcg._config._cg_dir

# get initialize_overture and finalize_overture in a safe way
try:
    initialize_overture = Grids.initialize_overture
    finalize_overture = Grids.finalize_overture
except Exception:
    pass
