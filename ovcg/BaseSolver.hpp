#ifndef _OVCG_BASESOLVER_HPP
#define _OVCG_BASESOLVER_HPP 1

// We use CRTP for static polymorphism
#include "debug.hpp"
#include "defines.hpp"

#include "common/common.hpp"

#include <CgSolverUtil.h>
#include <GenericGraphicsInterface.h>
#include <MpParameters.h>
#include <Oges.h>
#include <Ogshow.h>
#include <ParallelUtility.h>
#include <Regrid.h>

// solvers
#include <Cgins.h>
#include <Cgmp.h>

#include <cmath>
#include <string>

namespace ovcg {

//*************************************************************
//
// This base serves like an abstract base except the template
// here is the list of "abstract" member methods, note that
// "implicit/<i>" means the method is in derived class, while
// "explicit/<e>" for the opposite.
//
//    [1] <e> read_cmd
//    [2] <e> init_grid
//    [3] <e> save_solutions
//    [4] <e> get_total_steps
//    [5] <e> get_grids
//    [6] <i> init_solver
//    [7] <i> advance
//    [8] <i> finalize_solver
//
// Here are the things needed to by defined in derived solver
// and SolverFactory so that they can be then explicitly used
// in the base class:
//
//  [1] solver_t (CG solver classes, e.g. Cgmp, Cgins, etc)
//  [2] name (const static string, e.g. "cgmp", "cgins", etc)
//
// if later on we want to write C++ programs for the solver,
// we can write template methods like the following one:
//
//  template<class DerivedSolver, int Solver>
//  void interface(BaseSolver<DerivedSolver, Solver> &_solver) {
//    DerivedSolver &solver = _solver.self(); // cast to derived
//    // then all methods are explicit for solver instance
//  }
//
// NOTE CRTP is not needed if we just want to wrap it for python,
// but CRTP gives us some future-proof if we want to use these template
// stuffs in C++. Also, it allows us to call derived methods from
// base class, which can be helpful for the future development.
// However, a template base type is must, because even though some
// methods in cg solvers are not virtualized from DomainSolver, which
// is the base solver class for all cg solvers. This means that if
// we use a DomainSolver pointer and runtime new Cg solvers, it will
// not work for methods that only belong to Cg solvers. So we need
// our base solver class to hold the exact cg solver type on compile
// time and this can be done with template mechanism.
//
//************************************************************

// using a type trait, for solver type configurations
template <int Solver> struct SolverFactory { typedef void solver_t; };

// NOTE in general Solver should be defined as enum for clearness
// but since the base class is a friend in CompositeGridHelper,
// so if we use enum, then the enum must be defined before the
// CompositeGridHelper, which can mess up the structure of the code
// so we use integer here.

#ifdef OVCG_CGMP
#undef OVCG_CGMP
#endif
#ifdef OVCG_CGINS
#undef OVCG_CGINS
#endif
#ifdef OVCG_CGCNS
#undef OVCG_CGCNS
#endif

#define OVCG_CGMP 0
#define OVCG_CGINS 1
#define OVCG_CGCNS 2

// cgmp
template <> struct SolverFactory<OVCG_CGMP> { typedef ::Cgmp solver_t; };
// cgins
template <> struct SolverFactory<OVCG_CGINS> { typedef ::Cgins solver_t; };
// TODO remember to add header files and more factories when we
// wrap more solvers

// base class
template <class DerivedSolver, int Solver> class BaseSolver {
public:
  // get solver type from solverfactory
  // in each solver, a name should be defined
  typedef typename SolverFactory<Solver>::solver_t solver_t;

  // get the derived name
  const static std::string name;

  // casting
  const DerivedSolver &self() const OVCG_NOEXCEPT {
    return *static_cast<const DerivedSolver *>(this);
  }

  DerivedSolver &self() OVCG_NOEXCEPT {
    return *static_cast<DerivedSolver *>(this);
  }

  // Constructor
  explicit BaseSolver() OVCG_NOEXCEPT : _ps(nullptr),
                                        _solver(nullptr),
                                        _t(0.0),
                                        _grid_file(""),
                                        _show_file(""),
                                        _show_changed(true) {
    int    argc(0);
    char **argv_dummy = nullptr;
    Overture::start(argc, argv_dummy);
    INIT_PETSC_SOLVER();

    // initialize the dummy GUI
    // NOTE:
    //  [1] Global GUI maintain a stack of cmd files;
    //  [2] GUI will call perl parser routines
    argc         = 3;
    char *argv[] = {const_cast<char *>(name.c_str()),
                    const_cast<char *>("noplot"),
                    const_cast<char *>("nopause")};
    _ps          = Overture::getGraphicsInterface(name, false, argc, argv);
    _ps->turnOffGraphics();

    // default log files
    aString log_file = name + ".cmd";
    _ps->saveCommandFile(log_file);
    _ps->appendToTheDefaultPrompt(name + ">");
  }

  // destructor
  ~BaseSolver() OVCG_NOEXCEPT {
    if (_solver)
      delete _solver;
    Overture::finish();
  }

  // read in cmd
  void read_cmd(const std::string &file) OVCG_NOEXCEPT {
    aString cmd_file(file);
    _ps->readCommandFile(cmd_file);
  }

  // initialize grids
  void init_grid() OVCG_NOEXCEPT {
    // some default parameters in main programs
    bool      loadBalance                    = false;
    const int maxWidthExtrapInterpNeighbours = 4, numberOfParallelGhost = 2;
    aString   nameOfGridFile =
        readOrBuildTheGrid(*_ps, _cg._cg, loadBalance, numberOfParallelGhost,
                           maxWidthExtrapInterpNeighbours);
    _cg._cg.update(CompositeGrid::THEusualSuspects | CompositeGrid::THEdomain);
    _grid_file = std::string(nameOfGridFile);
    _gd.resize(_cg.get_component_grid_number());
    for (int grid = 0; grid < _cg.get_component_grid_number(); ++grid)
      _gd[grid].reference(grid, _cg);
  }

  // save solution
  void save_solutions(const std::string &file) OVCG_NOEXCEPT {
    if (_show_file != file) {
      _show_file    = file;
      _show_changed = true;
    }
    _check_or_init_show_file();
    _solver->saveShow(_solver->gf[_solver->current]);
  }

  // get total steps used
  int get_total_steps() const OVCG_NOEXCEPT {
    return _solver->parameters.dbase.template get<int>("globalStepNumber");
  }

  // get our grids helper
  CompositeGridHelper &get_grids() OVCG_NOEXCEPT { return _cg; }

protected:
  // initialize solver
  void _init_solver(int debug, std::string logname = "") OVCG_NOEXCEPT {
    // default arguments from main programs of cg
    bool    plotOption = false;
    Ogshow *show       = nullptr;
    _solver            = new solver_t(_cg._cg, _ps, show, plotOption);
    _solver->setNameOfGridFile(aString(_grid_file));
    if (logname == "")
      logname = name;
    _solver->parameters.openLogFiles(logname);
    _solver->setParametersInteractively();

    // default = -1, then use option from cmd file
    if (debug >= 0)
      _solver->parameters.dbase.template get<int>("debug") = debug;

    // configure show time at beginning
    show = _solver->parameters.dbase.template get<Ogshow *>("show");
    if (show) {
      _show_file    = std::string(show->getShowFileName());
      _show_changed = false;
    }
  }

  // protected members, may be some of these can be in private?
protected:
  GenericGraphicsInterface *_ps;
  CompositeGridHelper       _cg;
  solver_t *                _solver;
  double                    _t;
  std::string               _grid_file, _show_file;
  bool                      _show_changed;
  std::vector<GeometryData> _gd;

  // private members
private:
  // check/initialize show
  void _check_or_init_show_file() OVCG_NOEXCEPT {
    if (_show_changed) {
      // FIXME safe? or even correct? How overture manage Ogshow system?
      // NOTE not safe, need to check if null or not
      if (_solver->parameters.dbase.template get<Ogshow *>("show"))
        delete _solver->parameters.dbase.template get<Ogshow *>("show");
      Ogshow *&show = _solver->parameters.dbase.template get<Ogshow *>("show");
      show = new Ogshow(_show_file); // several args set default, see Ogshow.h
      _show_changed = false;
    }
  }
};

template <class DerivedSolver, int Solver>
const std::string BaseSolver<DerivedSolver, Solver>::name = DerivedSolver::name;

} // namespace ovcg

#endif // _OVCG_BASESOLVER_HPP
