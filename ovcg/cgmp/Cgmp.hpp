#ifndef _OVCG_CGMP_HPP
#define _OVCG_CGMP_HPP 1

#include "../BaseSolver.hpp"

// adjust ForDomain macro for our case
#define ForDomainOVCG(d)                                                       \
  for (int d = 0; d < _solver->domainSolver.size(); ++d)                       \
    if (_solver->domainSolver[d])

namespace ovcg {

// a wrapper of Cgmp with reading in command files
// and holding a piece of mesh

class CGmp : public BaseSolver<CGmp, OVCG_CGMP> {
public:
  // "my" name
  const static std::string name;

  // Constructor
  explicit CGmp() OVCG_NOEXCEPT : BaseSolver<CGmp, OVCG_CGMP>() {}

  // initial solver
  void init_solver(int debug = -1) OVCG_NOEXCEPT {
    _init_solver(debug);

    // cgmp routines
    _solver->cycleZero();

    // init subiteration numbers
    if (!_solver->parameters.dbase.has_key("numberOfSubSteps"))
      _solver->parameters.dbase.put<int>("numberOfSubSteps");
    _solver->parameters.dbase.get<int>("numberOfSubSteps") = 1;

    // init steps taken
    int &nost = _solver->numberOfStepsTaken;
    nost = std::max(0, nost);

    // init current time
    _t = static_cast<double>(_solver->gf[_solver->current].t);
  }

  // solve a step
  void advance(double t, double dt) OVCG_NOEXCEPT {
    // set time
    OVCG_ASSERT(t > _t, "Invalid time point.")
    _solver->parameters.dbase.get<real>("nextTimeToPrint") =
        _solver->parameters.dbase.get<real>("tFinal") = t;

    // old dt
    real dtOld = _solver->dt;
    // compute new dt
    real dtNew = _solver->getTimeStep(_solver->gf[_solver->current]);
    dtNew = std::min(dt, dtNew);
    OVCG_ASSERT(dtNew > 0.0, "Negative time step.");
    // adjuect dt and substeps, cg/common/src/computeSteps.C
    int &noss = _solver->parameters.dbase.get<int>("numberOfSubSteps");
    _solver->computeNumberOfStepsAndAdjustTheTimeStep(_t, t, t, noss, dtNew);
    _solver->dt = dtNew;
    // set each sub domian's dt
    ForDomainOVCG(d) { _solver->domainSolver[d]->dt = dtNew; }

    // check if timestep changed, from solve.C
    bool is_dt_ch = std::abs(dtNew - dtOld) > dtOld * REAL_EPSILON * 100.0;
    _solver->parameters.dbase.get<bool>("timeStepHasChanged") = is_dt_ch;

    // actual advance
    _solver->setupAdvance();
    _solver->advance(t);
    _t = static_cast<double>(_solver->gf[_solver->current].t);
    _solver->finishAdvance();
  }

  // finalize solver
  void finalize_solver() OVCG_NOEXCEPT {
    // Copy from solve.C
    ForDomainOVCG(d) {
      Parameters &parameters = _solver->domainSolver[d]->parameters;
      if (parameters.dbase.get<Ogshow *>("show") &&
          (!parameters.dbase.get<Ogshow *>("show")->isLastFrameInSubFile() ||
           !parameters.dbase.get<bool>("saveSequencesEveryTime"))) {
        _solver->domainSolver[d]->saveSequencesToShowFile();
        if (parameters.isMovingGridProblem())
          parameters.dbase.get<MovingGrids>("movingGrids").saveToShowFile();
      }
    }
  }
};

// define name
const std::string CGmp::name = "cgmp";

} // namespace ovcg

#undef ForDomainOVCG

#endif // _OVCG_CGMP_HPP
