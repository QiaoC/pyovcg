"""CGMP header"""

from libcpp.string cimport string as std_string
from ovcg.common cimport common


cdef extern from 'Cgmp.hpp' namespace 'ovcg':

    cdef cppclass CGmp:
        CGmp()
        void read_cmd(const std_string & file)
        void init_grid()
        void init_solver(int debug)
        void advance(double t, double dt)
        void save_solutions(const std_string & file)
        int get_total_steps()
        void finalize_solver()
        common.CompositeGridHelper &get_grids()

cdef class Cgmp:
    # hold a raw pointer of CGmp
    cdef CGmp *_inst
