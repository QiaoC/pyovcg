"""CGMP source file

This is the Cython source file for the definations
of ovcg::CGmp.
"""

from libcpp.string cimport string as std_string
from ovcg.common.grids cimport Grids
from ovcg.common cimport common
from cython.operator cimport dereference as deref
from ovcg.cgmp cimport cgmp


cdef class Cgmp:
    """Cgmp source definations"""

    def __cinit__(self):
        """Constructor, this will initialize a CGmp instance"""
        self._inst = new cgmp.CGmp()

    def __dealloc__(self):
        """Destructor"""
        del self._inst

    def read_cmd(self, str filename):
        """Read in cmd file"""
        cdef std_string fn = <std_string> filename.encode('UTF-8')
        self._inst.read_cmd(fn)

    def init_grid(self):
        """Initialize the mesh"""
        self._inst.init_grid()

    def init_solver(self, debug=-1):
        """Initialize the solver

        if debug < 0, then use debug option in cmd file,
        otherwise, change the debug option to <debug>
        """
        cdef int dbg = <int> debug
        self._inst.init_solver(dbg)

    def advance(self, t, dt):
        """Solve one step to time t, with potential value dt"""
        cdef double tt = <double> t
        cdef double dtt = <double> dt
        self._inst.advance(tt, dtt)

    def save_solutions(self, str filename):
        """Save solution to the <show> file"""
        cdef std_string fn = <std_string> filename.encode('UTF-8')
        self._inst.save_solutions(fn)

    def get_total_steps(self):
        """Get the overall steps, including subiterations, counts"""
        return self._inst.get_total_steps()

    def finalize_solver(self):
        """Finalize solver"""
        self._inst.finalize_solver()

    def get_grids(self):
        """Get the underlying grids"""
        cdef Grids cg = Grids()
        cdef common.CompositeGridHelper *inst = \
            <common.CompositeGridHelper *> &self._inst.get_grids()
        inst._retrieve(deref(<common.CompositeGridHelper *> cg._inst))
        return cg
