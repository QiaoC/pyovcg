#ifndef _OVCG_DEFINES_HPP
#define _OVCG_DEFINES_HPP 1

#if __cplusplus >= 201103L
#define OVCG_NOEXCEPT noexcept
#else
#define OVCG_NOEXCEPT
#define nullptr 0
#endif

#endif // _OVCG_DEFINES_HPP
