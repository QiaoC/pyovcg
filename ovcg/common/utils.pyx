"""UTILITIES source file"""

from ovcg.common cimport common
cimport cython
import numpy as np
cimport numpy as np
from libcpp.vector cimport vector as std_vector

cdef extern from '<algorithm>' namespace 'std':
    int count[I, T](I first, I last, T v)
    O copy[I, O](I first, I last, O ofirst)


def filter_out_unused_points(np.ndarray[np.int32_t, ndim=1] mask not None):
    cdef int n = mask.size
    cdef int m = count(<int *> mask.data, <int *> mask.data + n, 1) + \
        count(<int *> mask.data, <int *> mask.data + n, -1)
    cdef np.ndarray[np.int32_t, ndim=1] p = np.empty(m, dtype=np.int32)
    common.filter_out_unused_points(n, <const int *> mask.data, <int *> p.data)
    return p


def filter_out_unused(
    np.ndarray[np.int32_t, ndim=1] mask not None,
    np.ndarray[np.int32_t, ndim=2] conn not None):
    """It's user's responsiblity to ensure the consistency between mask and conn"""
    cdef int n = mask.size
    cdef std_vector[int] new_conn
    cdef int nodes = conn.shape[1]
    cdef int elems = conn.shape[0]
    cdef int m
    cdef np.ndarray[np.int32_t, ndim=1] p
    cdef np.ndarray[np.int32_t, ndim=2] c
    assert nodes == 2 or nodes == 4 or nodes == 8
    m = count(<int *> mask.data, <int *> mask.data + n, 1) + \
        count(<int *> mask.data, <int *> mask.data + n, -1)
    p = np.empty(m, dtype=np.int32)
    common.filter_out_unused(
        n,
        <const int *> mask.data,
        elems,
        nodes,
        <const int *> conn.data,
        <int *> p.data,
        new_conn)
    out_size = new_conn.size() // nodes
    c = np.empty(shape=(out_size, nodes), dtype=np.int32)
    copy(new_conn.begin(), new_conn.end(), <int *> c.data)
    return p, c


def flip_interface_normals(np.ndarray[np.int32_t, ndim=2] conn not None):
    cdef int elems = conn.shape[0]
    cdef int nodes = conn.shape[1]
    assert nodes == 2 or nodes == 4
    common.flip_interface_normals(elems, nodes, <int *> conn.data)


def extract_solution_points(np.ndarray[np.int32_t, ndim=1] mask not None):
    return np.arange(mask.size)[np.where(mask == 1)]


def extract_interpolation_points(np.ndarray[np.int32_t, ndim=1] mask not None):
    return np.arange(mask.size)[np.where(mask == -1)]


def extract_unused_points(np.ndarray[np.int32_t, ndim=1] mask not None):
    return np.arange(mask.size)[np.where(mask == 0)]
