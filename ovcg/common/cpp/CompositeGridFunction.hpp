#ifndef _OVCG_COMMON_GRID_CPP_COMPOSITEGRIDFUNCTION_HPP
#define _OVCG_COMMON_GRID_CPP_COMPOSITEGRIDFUNCTION_HPP 1

#include "../../debug.hpp"
#include "../../defines.hpp"

#include "CompositeGrid.hpp"
#include "IndexBox.hpp"

#include <CompositeGridFunction.h>
#include <CompositeGridOperators.h>
#include <Interpolant.h>
#include <MappedGridFunction.h>
#include <Overture.h>
#include <arrayGetIndex.h>

#include <map>
#include <string>

// since we have known value loops, it's better to unroll them regardless
// what users specified.
#ifdef __GNUC__
#define OVCG_COMPILER 1
#else
#define OVCG_COMPILER 0
#endif

#if OVCG_COMPILER
#pragma GCC push_options
#pragma GCC optimize("unroll-loops")
#endif

// several helper macros

#ifdef UPDATE_ALL
#undef UPDATE_ALL
#endif
#ifdef GRIDS_ADDRESSES_MUST_MATCH
#undef GRIDS_ADDRESSES_MUST_MATCH
#endif
#ifdef VALID_COMP_RANGE
#undef VALID_COMP_RANGE
#endif

#define UPDATE_ALL(__r)                                                        \
  _cgf.updateToMatchGrid(_cg->_cg, __r[0], __r[1], __r[2], __r[3], __r[4],     \
                         __r[5], __r[6], __r[7])

#define GRIDS_ADDRESSES_MUST_MATCH                                             \
  OVCG_ASSERT(_cgf.getCompositeGrid(false) == &_cg->_cg,                       \
              "Grids addresses don't match.")

#define VALID_COMP_RANGE                                                       \
  OVCG_ASSERT(comp >= 0 && comp < MAX_RANK_NUMBERS,                            \
              "A function cannot have more than 5 components.")

namespace ovcg {
// A helper class for dealing with function instances
// in Overture. This is the core component for making
// Overture working with external codes.

// NOTE Overture stores a function with 8 dimensions
// u(x1, x2, x3, x4, x5, x6, x7, x8), we don't need to explicitly call this
// the interface is wired, usually, u(x1, x2, x3) = first compoent on x1, x2, x3
// u(x1, x2, x3, ?) ? = number of compoent
// However, it's also possible to mass up, say u(x1, ?, x2, x3) is allowed,
// so that's why Overture provides sa stands for standard argument, it's
// less efficient, but very general, i.e. u.sa(x1, x2, x3, 2) will always
// map to the 3rd componet at indices(x1, x2, x3)

class CompositeGridFunctionHelper {

  // We consider to define a function over three core
  // component:
  //    [1] an Overture function
  //    [2] an Overture function operators
  //    [3] an Overture interpolant
  // as well as a pointer to our wrapper for Overture cg
  // Mote that item [3] is for performing interpolation
  // amount overset regions. This is the key for us
  // to ensure the consistency amoung grid pathces when
  // we have some external data written in.

  // NOTE for dealing with values, we will work with
  // each function associated with each mapped grid.
  // This is done internally, users need to specify
  // the grid number.

public:
  // 8 and 5
  enum {
    MAX_INDEX_NUMBERS = realCompositeGridFunction::maximumNumberOfIndicies,
    MAX_RANK_NUMBERS = realCompositeGridFunction::maximumNumberOfComponents
  };

  // Constructors
  explicit CompositeGridFunctionHelper() OVCG_NOEXCEPT {}

  // passing in cg
  explicit CompositeGridFunctionHelper(CompositeGridHelper *cg)
      : _cg(cg), _cgf(cg->_cg), _op(cg->_cg), _interp(cg->_cg) {
    _cgf.setOperators(_op);
    _cgf.setInterpolant(&_interp);
  }

  // an optional rank, a state vectors may store things like
  // temp, pressure, u, v, w, up to 5 components
  CompositeGridFunctionHelper(CompositeGridHelper *cg, int rank)
      : _cg(cg), _op(cg->_cg), _interp(cg->_cg) {
    OVCG_ASSERT(rank <= MAX_RANK_NUMBERS,
                "A function cannot have more than 5 fields.");
    Range xyz = nullRange;
    Range f = Range(0, rank < 0 ? 0 : rank);
    _cgf.updateToMatchGrid(*_cg->_cg, xyz, xyz, xyz, f);
    _cgf.setOperators(_op);
    _cgf.setInterpolant(&_interp);
  }

  // utilities
  bool has_operators() const OVCG_NOEXCEPT {
    return _cgf.getOperators() != nullptr;
  }
  bool has_composite_grids() const OVCG_NOEXCEPT {
    return _cgf.getCompositeGrid() != nullptr;
  }
  bool has_interpolant() const OVCG_NOEXCEPT {
    return _cgf.getInterpolant(false) != nullptr;
  }

  // get methods
  int get_dimensions() const OVCG_NOEXCEPT { return _cg->get_dimensions(); }
  int get_operator_order_of_accuracy() const OVCG_NOEXCEPT {
    return _op.getOrderOfAccuracy();
  }
  int get_grid_number() const OVCG_NOEXCEPT { return _cg->_cg.numberOfGrids(); }
  int get_component_grid_number() const OVCG_NOEXCEPT {
    return _cg->_cg.numberOfComponentGrids();
  }

  int get_rank() const OVCG_NOEXCEPT { return _cgf.getNumberOfComponents(); }
  int get_component_number_by_name(const std::string &name) const
      OVCG_NOEXCEPT {
    // this function will return -1 if mastser name
    std::map<std::string, int>::const_iterator iter = _name2id.find(name);
    OVCG_ASSERT(iter != _name2id.end(), "Invalid field name.");
    return iter->second;
  }
  std::string get_component_name(int comp = -1) const OVCG_NOEXCEPT {
    if (comp < 0)
      return std::string(_cgf.getName());
    VALID_COMP_RANGE;
    return std::string(_cgf.getName(comp));
  }

  // get grids
  const CompositeGridHelper &get_grids() const OVCG_NOEXCEPT {
    GRIDS_ADDRESSES_MUST_MATCH;
    return *_cg;
  }
  CompositeGridHelper &get_grids() OVCG_NOEXCEPT {
    GRIDS_ADDRESSES_MUST_MATCH;
    return *_cg;
  }

  // set methods
  void set_composite_grids(CompositeGridHelper &cg) {
    _cg = &cg;
    Range r[MAX_INDEX_NUMBERS];
    for (int i = 0; i < MAX_INDEX_NUMBERS; ++i)
      r[i] = nullRange;

    // update function
    UPDATE_ALL(r);
    // update interpolant
    _interp.updateToMatchGrid(_cg->_cg);
    // update operator
    _op.updateToMatchGrid(_cg->_cg);
  }

  // set name of functions fields
  void set_component_name(const std::string &name,
                          int comp = -1) OVCG_NOEXCEPT {
    aString n = name;
    if (comp < 0) {
      _cgf.setName(n);
      _name2id[name] = comp;
      return;
    }
    OVCG_ASSERT(comp < get_rank(), "Exceeding maximum component numbers.");
    _cgf.setName(n, comp);
    // assume no same names
    _name2id[name] = comp;
  }

  // set order of accuracy, this is advanced option
  // firstly, only 2 or 4 is supported (FIXME?)
  // secondly, you need to make sure the grids support 4 order
  void set_operator_order_of_accuracy(int order) OVCG_NOEXCEPT {
    _op.setOrderOfAccuracy(order);
  }

  // computational routines

  // interpolation
  // this is for interpolate overlapping regions from discretization
  // points value in a grid to the overlapped grid's interpolation
  // regions. This is how Overture makes internal value consistency
  void interpolate() { _cgf.interpolate(); }

  // get function values
  // retrieving the default function value from this function space
  void get_values(int grid, const IndexBox &i, void *out,
                  int comp = 0) const OVCG_NOEXCEPT {
    OVCG_ASSERT(grid < get_grid_number(), "Invalid grid number.");
    OVCG_ASSERT(grid > -1, "Invalid grid number.");
    OVCG_ASSERT(comp < get_rank(), "Invalid component number.");
    OVCG_ASSERT(comp > -1, "Invalid component number.");

    // low-level casting
    real *x = reinterpret_cast<real *>(out);

    // for convinient
    realMappedGridFunction &u = _cgf[grid];

    int count = 0;
    const int incx = i.increment(0), incy = i.increment(1),
              incz = i.increment(2);
    for (int k = i.first(2); k <= i.last(2); k += incz)
      for (int j = i.first(1); j <= i.last(1); j += incy)
        for (int t = i.first(0); t <= i.last(0); t += incx, ++count)
          x[count] = u.sa(t, j, k, comp);
  }

  // get a component value by name
  void get_component_values(int grid, const std::string &name,
                            const IndexBox &i, void *out) const OVCG_NOEXCEPT {
    int comp = get_component_number_by_name(name);
    if (comp < 0)
      comp = 0;
    get_values(grid, i, out, comp);
  }

  // compute gradients
  // we will store in [[u0_x, u0_y, u0_z]...[ui_x, ui_y, ui_z]]
  void get_grad(int grid, const IndexBox &i, void *out,
                int comp = 0) const OVCG_NOEXCEPT {
    OVCG_ASSERT(grid < get_grid_number(), "Invalid grid number.");
    OVCG_ASSERT(grid > -1, "Invalid grid number.");
    OVCG_ASSERT(comp < get_rank(), "Invalid component number.");
    OVCG_ASSERT(comp > -1, "Invalid component number.");
    // has to create buffer
    realMappedGridFunction grad =
        _cgf[grid].grad(i._index[0], i._index[1], i._index[2], comp);
    const int dim = get_dimensions();

    // low-level casting
    real *x = reinterpret_cast<real *>(out);

    int count = 0;
    const int incx = i.increment(0), incy = i.increment(1),
              incz = i.increment(2);
    for (int k = i.first(2); k <= i.last(2); k += incz)
      for (int j = i.first(1); j <= i.last(1); j += incy)
        for (int t = i.first(0); t <= i.last(0); t += incx)
          for (int d = 0; d < dim; ++d, ++count)
            x[count] = grad(t, j, k, d); // hope this is right
  }

  // get gradient by name
  void get_component_grad(int grid, const std::string &name, const IndexBox &i,
                          void *out) const OVCG_NOEXCEPT {
    int comp = get_component_number_by_name(name);
    if (comp < 0)
      comp = 0;
    get_grad(grid, i, out, comp);
  }

private:
  // function instance
  realCompositeGridFunction _cgf;
  // operators
  CompositeGridOperators _op;
  // interpolant operator
  Interpolant _interp;
  // our help cg
  CompositeGridHelper *_cg;
  // a string to int map
  std::map<std::string, int> _name2id;

  // internal reference functions for solvers
  // the input is handled by developer and retrieved from mature
  // CG solvers, which means it should contain everything (core
  // compoenents in it), we then reference them.
  void _reference_everything(realCompositeGridFunction &cgf);

  // updating name2id maps
  void _update_name2id() {
    const static std::string nullstr = std::string(blankString);
    const int n = get_rank();
    std::string name;
    // including master function space
    for (int i = 0; i <= n; ++i) {
      name = get_component_name(i - 1);
      if (name != nullstr)
        _name2id[name] = i - 1;
    }
  }
};

} // namespace ovcg

#if OVCG_COMPILER
#pragma GCC pop_options
#endif

#undef OVCG_COMPILER

#endif // _OVCG_COMMON_GRID_CPP_COMPOSITEGRIDFUNCTION_HPP
