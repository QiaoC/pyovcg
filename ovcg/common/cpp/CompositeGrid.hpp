#ifndef _OVCG_COMMON_GRID_CPP_COMPOSITEGRID_HPP
#define _OVCG_COMMON_GRID_CPP_COMPOSITEGRID_HPP 1

#include "../../debug.hpp"
#include "../../defines.hpp"
#include <cstdint>

#include "IndexBox.hpp"

#include <CompositeGrid.h>
#include <MappedGrid.h>
#include <Overture.h>
#include <arrayGetIndex.h>

#ifdef VALID_AXIS
#undef VALID_AXIS
#endif
#define VALID_AXIS axis >= 0 && axis < IndexBox::MAX_DIMENSION

#ifdef VALID_SIDE
#undef VALID_SIDE
#endif
#define VALID_SIDE side == 0 || side == 1

namespace ovcg {

// A helper class for CompositeGrid in Overture
// This one will be later embedded into each solver
// use this object after initialize the solver!

// TODO make multigrid supported

// Also, we only use structured grid!

class CompositeGridHelper {
public:
  explicit CompositeGridHelper() OVCG_NOEXCEPT {}
  ~CompositeGridHelper() OVCG_NOEXCEPT {}

  // initializations
  void read_hdf(const std::string &file) {
    aString filename(file);
    getFromADataBase(_cg, filename);
    _cg.update(CompositeGrid::THEusualSuspects | CompositeGrid::THEdomain);
  }

  // get methods
  int get_dimensions() const OVCG_NOEXCEPT { return _cg.numberOfDimensions(); }
  int get_gridpoint_number() const OVCG_NOEXCEPT {
    return _cg.numberOfGridPoints();
  }
  int get_domain_number() const OVCG_NOEXCEPT { return _cg.numberOfDomains(); }
  int get_grid_number() const OVCG_NOEXCEPT { return _cg.numberOfGrids(); }
  int get_component_grid_number() const OVCG_NOEXCEPT {
    return _cg.numberOfComponentGrids();
  }
  int get_multigrid_level_number() const OVCG_NOEXCEPT {
    return _cg.numberOfMultigridLevels();
  }
  std::string get_domain_name(int domain) const OVCG_NOEXCEPT {
    OVCG_ASSERT(domain < get_domain_number(), "Out of domain range");
    return std::string(_cg.getDomainName(domain));
  }

  // jump into a patch, get methods
  void get_indices(int grid, IndexBox &i, bool has_ghost = false) const {
    OVCG_ASSERT(grid < get_grid_number(), "Out of grid range");
    const MappedGrid &mg = _cg[grid];
    if (has_ghost)
      getIndex(mg.dimension(), i._index[0], i._index[1], i._index[2]);
    else
      getIndex(mg.gridIndexRange(), i._index[0], i._index[1], i._index[2]);
  }

  // grid is the grid number
  // get a boundary index out
  // side is [0:1], 0 start, 1 end
  // axis is [0:2], 0 x, 1 y, 2 z
  void get_boundary_indices(int grid, int side, int axis, IndexBox &i) const {
    OVCG_ASSERT(VALID_SIDE, "Invalid side range [0:1].");
    OVCG_ASSERT(VALID_AXIS, "Invalid axis range [0:2].");
    OVCG_ASSERT(grid < get_grid_number(), "Out of grid range");
    // get the mappedgrid component
    const MappedGrid &mg = _cg[grid];
    getBoundaryIndex(mg.gridIndexRange(), side, axis, i._index[0], i._index[1],
                     i._index[2]);
  }

  // grid is the grid number
  // get ghost lines
  // ghost = -1, 0, 1, 2 (inner, boundary+ghost, outer1, outer2)
  void get_ghost_indices(int grid, int side, int axis, IndexBox &i,
                         int ghost = 1) const {
    OVCG_ASSERT(grid < get_grid_number(), "Out of grid range");
    OVCG_ASSERT(VALID_SIDE, "Invalid side range [0:1].");
    OVCG_ASSERT(VALID_AXIS, "Invalid axis range [0:2].");
    OVCG_ASSERT(ghost >= -1 && ghost <= 2, "Invalid ghost line range [-1:2].");
    // get the mappedgrid component
    const MappedGrid &mg = _cg[grid];
    getGhostIndex(mg.dimension(), side, axis, i._index[0], i._index[1],
                  i._index[2], ghost);
  }

  // grid accessing
  void get_grid_tags(int *out) const OVCG_NOEXCEPT {
    const int n = get_grid_number();
    for (int i = 0; i < n; ++i)
      out[i] = _cg.gridNumber(i);
  }

  void get_domain_tags(int *out) const OVCG_NOEXCEPT {
    const int n = get_grid_number();
    for (int i = 0; i < n; ++i)
      out[i] = _cg.domainNumber(i);
  }

  // get domain
  void get_domain(int d, CompositeGridHelper &cg) {
    _cg.domain[d].update(CompositeGrid::THEusualSuspects |
                         CompositeGrid::THEdomain);
    cg._cg.reference(_cg.domain[d]);
  }

  // stuffs for overlapping region and interpolations
  // TODO understanding the backward interpolation

  // get mask from a grid, in range i, where
  // for vertex V, if
  //  out[P] == 1, then it's a discretization point
  //  out[P] == 0, then it's unused
  //  out[P] == -1, then it's interpolation point, i.e.
  //    get data from other grid
  //  out[P] == -2, then it's ghost point
  //  out[P] == -3. others (unsupported yet)
  void get_mask(int grid, const IndexBox &i, int *out) {
    OVCG_ASSERT(grid < get_grid_number(), "Out of grid range");
    _cg.update(CompositeGrid::THEmask);
    const int incx = i.increment(0), incy = i.increment(1),
              incz = i.increment(2);
    const MappedGrid &mg = _cg[grid];
    int count = 0;
    for (int k = i.first(2); k <= i.last(2); k += incz)
      for (int j = i.first(1); j <= i.last(1); j += incy)
        for (int t = i.first(0); t <= i.last(0); t += incx, ++count) {
          const int flag = mg.mask()(t, j, k);
          if (flag & MappedGrid::ISusedPoint)
            out[count] =
                (flag & MappedGrid::ISdiscretizationPoint)
                    ? 1
                    : (flag & MappedGrid::ISinterpolationPoint)
                          ? -1
                          : (flag & MappedGrid::ISghostPoint) ? -2 : -3;
          else
            out[count] = 0;
        }
  }

  // interpolation points counts
  // expected return in three axis by number of grids array, eg
  //    [[2, 3],[1, 2],[3, 4]] means
  // in x-axis, first grid has 2 interpolation points, ...
  void get_interpolation_points(int *out) const OVCG_NOEXCEPT {
    const int n = get_grid_number();
    int count = 0;
    for (int axis = 0; axis < 3; ++axis)
      for (int i = 0; i < n; ++i, ++count)
        out[count] = _cg.numberOfInterpolationPoints(axis, i);
  }

  // get interpolation relations between compoenent grids
  // if true, then there might be interpolation happening
  bool may_interpolate(int from_grid, int to_grid,
                       int mlvl = 0) const OVCG_NOEXCEPT {
    OVCG_ASSERT(from_grid < get_component_grid_number(), "Out of grid range");
    OVCG_ASSERT(to_grid < get_component_grid_number(), "Out of grid range");
    return _cg.mayInterpolate(to_grid, from_grid, mlvl);
  }

  // get a list of grids index that may interpolate to a
  // a given component grid
  // the list terminate when reach -1
  void get_interpolation_grids(int *list, int to_grid,
                               int mlvl = 0) const OVCG_NOEXCEPT {
    const int n = get_component_grid_number();
    OVCG_ASSERT(to_grid < n, "Out of grid range");
    for (int i = 0; i < n; ++i)
      list[i] = _cg.interpolationPreference(i, to_grid, mlvl);
  }

  // get shared boundary flag
  // currently, based on my understanding, this should be a user-specified
  // flag that indicates two sets are shared
  // for instance, if we found grid1 and grid2 are overlapped,
  // then we may check this flag in the two grids to found the overlapped
  // interfaces, this should be physical interfaces
  int get_boundary_share_flag(int grid, int side,
                              int axis) const OVCG_NOEXCEPT {
    OVCG_ASSERT(grid < get_grid_number(), "Out of grid range");
    OVCG_ASSERT(VALID_SIDE, "Invalid side range [0:1].");
    OVCG_ASSERT(VALID_AXIS, "Invalid axis range [0:2].");
    return _cg[grid].sharedBoundaryFlag()(side, axis);
  }

  // here is the function for determine boundary type
  // where if value > 0, then physical domain
  // < 0 for periodic
  // ==0 for interpolation
  int get_boundary_condition_flag(int grid, int side,
                                  int axis) const OVCG_NOEXCEPT {
    OVCG_ASSERT(grid < get_grid_number(), "Out of grid range");
    OVCG_ASSERT(VALID_SIDE, "Invalid side range [0:1].");
    OVCG_ASSERT(VALID_AXIS, "Invalid axis range [0:2].");
    return _cg[grid].boundaryCondition()(side, axis);
  }

  // get the cutting holes information from domain boundaries
  bool may_cut_holes(int cutting, int cut) const OVCG_NOEXCEPT {
    OVCG_ASSERT(cutting < get_component_grid_number(), "Out of grid range");
    OVCG_ASSERT(cut < get_component_grid_number(), "Out of grid range");
    return _cg.mayCutHoles(cutting, cut);
  }

  // self-constructred, cutting grid list that contains the list
  // of grids cutting the cut grid
  void get_cutting_grids(int *list, int cut) const OVCG_NOEXCEPT {
    const int n = get_component_grid_number();
    OVCG_ASSERT(cut < n, "Out of grid range");
    int j = 0;
    for (int i = 0; i < n; ++i)
      if (_cg.mayCutHoles(i, cut))
        list[j++] = i;
    if (j < n)
      list[j] = -1;
  }

  void _retrieve(CompositeGridHelper &cg) OVCG_NOEXCEPT {
    cg._cg.reference(_cg);
  }

  void _retrieve_level(CompositeGridHelper &cg, int level) OVCG_NOEXCEPT {
    OVCG_ASSERT(level < get_multigrid_level_number(),
                "Out of multigrid level range.");
    _cg.update(CompositeGrid::THEmultigridLevel);
    cg._cg.reference(_cg.multigridLevel[level]);
  }

  // helper functions for testing
  static void initialize_overture() {
    int argc(0);
    char **argv = nullptr;
    Overture::start(argc, argv);
  }
  static void finalize_overture() { Overture::finish(); }

private:
  // hold a pointer to a cg
  CompositeGrid _cg;

  // add friend classes
  friend class GeometryData;
  friend class CompositeGridFunctionHelper;
  template <class DerivedSolver, int Solver> friend class BaseSolver;
  friend class CGins;
};

} // namespace ovcg

#endif // _OVCG_COMMON_GRID_CPP_COMPOSITEGRID_HPP
