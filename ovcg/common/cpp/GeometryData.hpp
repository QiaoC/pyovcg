#ifndef _OVCG_COMMON_GEOMETRY_CPP_GEOMETRYDATA_HPP
#define _OVCG_COMMON_GEOMETRY_CPP_GEOMETRYDATA_HPP 1

#include "../../debug.hpp"
#include "../../defines.hpp"
#include "CompositeGrid.hpp"
#include <string>

#include <MappedGrid.h>
#include <Overture.h>
#include <arrayGetIndex.h>

#ifdef VALID_AXIS
#undef VALID_AXIS
#endif
#define VALID_AXIS axis >= 0 && axis < IndexBox::MAX_DIMENSION

#ifdef VALID_SIDE
#undef VALID_SIDE
#endif
#define VALID_SIDE side == 0 || side == 1

namespace ovcg {

// This is a helper class for computing the geometry quantities
// currently, only supports retrieving data from grid

enum GeometryField {
  VERTEX = 0,
  VERTEX_DERIVATIVE,
  INVERSE_VERTEX_DERIVATIVE,
  VERTEX_JACOBIAN,
  VERTEX_BOUNDARY_NORMAL,
  BOUNDING_BOX,
  EVERYTHING,
  _LENGTH
};

class GeometryData {
public:
  // constructor
  explicit GeometryData() OVCG_NOEXCEPT {}
  GeometryData(int grid, CompositeGridHelper &cg) {
    _ref.reference(cg._cg[grid]);
  }
  ~GeometryData() OVCG_NOEXCEPT {}

  // reference
  void reference(int grid, CompositeGridHelper &cg) {
    _ref.reference(cg._cg[grid]);
  }

  // dimension
  int get_dimensions() const OVCG_NOEXCEPT { return _ref.numberOfDimensions(); }

  // computing/get methods

  // vertices
  // out output data,
  // i indices range
  void get_vertices(void *out, const IndexBox &i) {
    // low-level casting
    real *x = reinterpret_cast<real *>(out);
    int count = 0;
    const int incx = i.increment(0), incy = i.increment(1),
              incz = i.increment(2), dim = _ref.numberOfDimensions();
    _ref.update(MappedGrid::THEvertex);
    for (int k = i.first(2); k <= i.last(2); k += incz)
      for (int j = i.first(1); j <= i.last(1); j += incy)
        for (int t = i.first(0); t <= i.last(0); t += incx)
          for (int d = 0; d < dim; ++d, ++count)
            x[count] = _ref.vertex()(t, j, k, d);
  }

  // vertex direvative, either inverse or not, physical<->reference
  // in 2d
  // we have (x, y) -> (r, s),
  // then we store a derivative in 1x4 array of [x_r, x_s, y_r, y_s]
  // for inverse mapping,
  // we store them in the following order [r_x, r_y, s_x, s_y]
  void get_vertex_derivative(void *out, const ovcg::IndexBox &i,
                             bool inverse = false) {
    // low-level casting
    real *x = reinterpret_cast<real *>(out);
    int count = 0;
    const int incx = i.increment(0), incy = i.increment(1),
              incz = i.increment(2), dim = _ref.numberOfDimensions();
    if (!inverse) {
      _ref.update(MappedGrid::THEvertexDerivative);
      for (int k = i.first(2); k <= i.last(2); k += incz)
        for (int j = i.first(1); j <= i.last(1); j += incy)
          for (int t = i.first(0); t <= i.last(0); t += incx)
            for (int d1 = 0; d1 < dim; ++d1)
              for (int d2 = 0; d2 < dim; ++d2, ++count)
                x[count] = _ref.vertexDerivative()(t, j, k, d1, d2);
    } else {
      _ref.update(MappedGrid::THEinverseVertexDerivative);
      for (int k = i.first(2); k <= i.last(2); k += incz)
        for (int j = i.first(1); j <= i.last(1); j += incy)
          for (int t = i.first(0); t <= i.last(0); t += incx)
            for (int d1 = 0; d1 < dim; ++d1)
              for (int d2 = 0; d2 < dim; ++d2, ++count)
                x[count] = _ref.inverseVertexDerivative()(t, j, k, d1, d2);
    }
  }

  // vertex jacobian
  // store det(X_R)
  void get_vertex_jacobian(void *out, const IndexBox &i) {
    // low-level casting
    real *x = reinterpret_cast<real *>(out);
    int count = 0;
    const int incx = i.increment(0), incy = i.increment(1),
              incz = i.increment(2);
    _ref.update(MappedGrid::THEvertexJacobian);
    for (int k = i.first(2); k <= i.last(2); k += incz)
      for (int j = i.first(1); j <= i.last(1); j += incy)
        for (int t = i.first(0); t <= i.last(0); t += incx, ++count)
          x[count] = _ref.vertexJacobian()(t, j, k);
  }

  // get outward boundary normal
  void get_boundary_normal(int side, int axis, void *out, const IndexBox &i) {
    OVCG_ASSERT(VALID_SIDE, "Invalid side range [0:1].");
    OVCG_ASSERT(VALID_AXIS, "Invalid axis range [0:2].");
    // low-level casting
    real *x = reinterpret_cast<real *>(out);
    int count = 0;
    const int incx = i.increment(0), incy = i.increment(1),
              incz = i.increment(2), dim = _ref.numberOfDimensions();
    _ref.update(MappedGrid::THEvertexBoundaryNormal);
    const RealMappedGridFunction &bn = _ref.vertexBoundaryNormal(side, axis);
    for (int k = i.first(2); k <= i.last(2); k += incz)
      for (int j = i.first(1); j <= i.last(1); j += incy)
        for (int t = i.first(0); t <= i.last(0); t += incx)
          for (int d = 0; d < dim; ++d, ++count)
            x[count] = bn(t, j, k, d);
  }

  // get bounding box
  // [x_0, y_0, z_0]
  // [x_1, y_1, z_1]
  // no ghost!
  void get_bounding_box(void *out) {
    // low-level casting
    real *x = reinterpret_cast<real *>(out);
    _ref.update(MappedGrid::THEboundingBox);
    const int dim = _ref.numberOfDimensions();
    for (int i = 0; i < dim; ++i) {
      x[i] = _ref.boundingBox()(0, i);
      x[dim + i] = _ref.boundingBox()(1, i);
    }
  }

  // destroy a field to release memory
  void destroy_field(int f) {
    OVCG_ASSERT(f >= 0 && f < _LENGTH, "Invalid choice of field quantity.");
    _ref.destroy(GeometryData::_field[f]);
  }

  // TODO [1] support cell-center; [2] add set methods (maybe?)

private:
  // A pointer to a specific mapped grid
  MappedGrid _ref;

  static int _field[_LENGTH];
};

int GeometryData::_field[] = {
    MappedGrid::THEvertex,
    MappedGrid::THEvertexDerivative,
    MappedGrid::THEinverseVertexDerivative,
    MappedGrid::THEvertexJacobian,
    MappedGrid::THEvertexBoundaryNormal,
    MappedGrid::THEboundingBox,
    _field[0] | _field[1] | _field[2] | _field[3] | _field[4] | _field[5],
};

} // namespace ovcg

#endif // _OVCG_COMMON_GEOMETRY_CPP_GEOMETRYDATA_HPP
