#ifndef _OVCG_COMMON_INDEX_CPP_INDEX_HPP
#define _OVCG_COMMON_INDEX_CPP_INDEX_HPP 1

#include "../../debug.hpp"
#include "../../defines.hpp"
#include <Overture.h>

#ifdef VALID_AXIS
#undef VALID_AXIS
#endif
#define VALID_AXIS axis >= 0 && axis < MAX_DIMENSION

// since we have known value loops, it's better to unroll them regardless
// what users specified.
#ifdef __GNUC__
#define OVCG_COMPILER 1
#else
#define OVCG_COMPILER 0
#endif

#if OVCG_COMPILER
#pragma GCC push_options
#pragma GCC optimize("unroll-loops")
#endif

namespace ovcg {
// a index box that contains the range of three dimensions
// structured finite difference indices
class IndexBox {
public:
  enum { MAX_DIMENSION = 3 };

  // constructors
  explicit IndexBox() OVCG_NOEXCEPT {}
  explicit IndexBox(const IndexBox &other) OVCG_NOEXCEPT {
    for (int i = 0; i < MAX_DIMENSION; ++i)
      _index[i] = other._index[i];
  }

  // Get the begining index
  int first(int axis) const OVCG_NOEXCEPT {
    OVCG_ASSERT(VALID_AXIS, "Invalid axis range [0:2].");
    return _index[axis].getBase();
  }

  // Get the ending index
  int last(int axis) const OVCG_NOEXCEPT {
    OVCG_ASSERT(VALID_AXIS, "Invalid axis range [0:2].");
    return _index[axis].getBound();
  }

  // Get the increment
  int increment(int axis) const OVCG_NOEXCEPT {
    OVCG_ASSERT(VALID_AXIS, "Invalid axis range [0:2].");
    return _index[axis].getStride();
  }

  // Get the size, counting increment
  int size(int axis) const OVCG_NOEXCEPT {
    OVCG_ASSERT(VALID_AXIS, "Invalid axis range [0:2].");
    return _index[axis].length();
  }

  // Get the total number of nodes
  int get_nodes_number() const OVCG_NOEXCEPT {
    return size(0) * size(1) * size(2);
  }

  // make a copy
  void assign(const IndexBox &other) OVCG_NOEXCEPT {
    for (int i = 0; i < MAX_DIMENSION; ++i)
      _index[i] = other._index[i];
  }

  // set a range for a specific axis
  void set(int axis, int first, int last, int inc = 1) OVCG_NOEXCEPT {
    OVCG_ASSERT(VALID_AXIS, "Invalid axis range [0:2].");
    _index[axis] = Index(first, last, inc);
  }

  // connectivity maker

  // connectivity maker,
  // because we assume structured grids, so we can generate connectivity
  // table just based on indices
  // arg1 dimension
  // arg2 output connectivity table
  // arg3 flag to indicate Q1 (true) or P1
  void generate_surface_connectivity(int d, int *conn,
                                     bool q1 = true) const OVCG_NOEXCEPT {
    OVCG_ASSERT(d > 0 && d < 4, "Invalid dimension range [1:3].");
    int element = 0;
    if (d == 1) {
      conn[element] = 0;
      return;
    }
    const int n = get_nodes_number();
    int orientation[4];
    if (d == 2) {
      // 2 dimension, edges of structured grid
      orientation[0] = 0;
      orientation[1] = 1;
      for (int j = 0; j < n - 1; ++j)
        for (int k = 0; k < 2; ++k, ++element)
          conn[element] = j + orientation[k];
      return;
    }
    // 3D, surface connectivity
    // if x->axis either Start or End, get y->axis length
    const int sz = size(0) == 1 ? size(0) * size(1) : size(0);
    OVCG_ASSERT(sz != n, "Line segment in 3D is not supported.");
    const int lines = n / sz;
    orientation[0] = 0;
    orientation[1] = 1;
    orientation[2] = sz + 1;
    orientation[3] = sz;
    if (q1) { // quadralitural
      for (int p = 0; p < lines - 1; ++p) {
        const int leading = p * sz;
        for (int j = 0; j < sz - 1; ++j)
          for (int k = 0; k < 4; ++k, ++element)
            conn[element] = leading + orientation[k] + j;
      }
    } else {
      for (int p = 0; p < lines - 1; ++p) {
        const int leading = p * sz;
        for (int j = 0; j < sz - 1; ++j) {
          const int &first = conn[element];
          for (int k = 0; k < 3; ++k, ++element)
            conn[element] = leading + orientation[k] + j;
          for (int k = 2; k < 4; ++k, ++element)
            conn[element] = leading + orientation[k] + j;
          conn[element] = first;
          ++element;
        }
      }
    }
  }

  // connectivity maker,
  // because we assume structured grids, so we can generate connectivity
  // table just based on indices
  // arg1 dimension
  // arg2 output connectivity table
  // arg3 flag to indicate Q1 (true) or P1
  // arg4 flag to indicate cut Q1 into 5 or 6 (default) P1s in 3D
  void generate_volume_connectivity(int d, int *conn, bool q1 = true,
                                    bool six_cuts = true) const OVCG_NOEXCEPT {
    static int six[24] = {0, 1, 3, 4, 1, 3, 4, 5, 4, 7, 5, 3,
                          1, 2, 3, 5, 2, 3, 5, 7, 2, 5, 6, 7},
               five[20] = {0, 1, 3, 4, 1, 2, 3, 6, 3, 4,
                           6, 7, 1, 3, 4, 6, 1, 4, 5, 6};
    OVCG_ASSERT(d > 0 && d < 4, "Invalid dimension range [1:3].");
    int element = 0;
    const int n = get_nodes_number();
    int orientation[8];
    orientation[0] = 0;
    orientation[1] = 1;
    // 1D
    if (d == 1) {
      for (int j = 0; j < n - 1; ++j)
        for (int k = 0; k < 2; ++k, ++element)
          conn[element] = j + orientation[k];
      return;
    }
    const int szx = size(0), szy = size(1);
    orientation[2] = szx + 1;
    orientation[3] = szx;
    // 2D
    if (d == 2) {
      if (q1) {
        for (int p = 0; p < szy - 1; ++p) {
          const int leading = p * szx;
          for (int j = 0; j < szx - 1; ++j)
            for (int k = 0; k < 4; ++k, ++element)
              conn[element] = leading + orientation[k] + j;
        }
      } else {
        for (int p = 0; p < szy - 1; ++p) {
          const int leading = p * szx;
          for (int j = 0; j < szx - 1; ++j) {
            const int &first = conn[element];
            for (int k = 0; k < 3; ++k, ++element)
              conn[element] = leading + orientation[k] + j;
            for (int k = 2; k < 4; ++k, ++element)
              conn[element] = leading + orientation[k] + j;
            conn[element] = first;
            ++element;
          }
        }
      }
      return;
    }
    const int szz = size(2);
    // using CGNS orientation
    for (int k = 4; k < 8; ++k)
      orientation[k] = orientation[k - 4] + szx * szy;
    // 3D
    if (q1) {
      for (int q = 0; q < szz - 1; ++q) {
        const int leading1 = q * szx * szy;
        for (int p = 0; p < szy - 1; ++p) {
          const int leading = p * szx + leading1;
          for (int j = 0; j < szx - 1; ++j)
            for (int k = 0; k < 8; ++k, ++element)
              conn[element] = leading + orientation[k] + j;
        }
      }
    } else {
      const int *cut = six_cuts ? six : five,
                local_node_size = six_cuts ? 24 : 20;
      for (int q = 0; q < szz - 1; ++q) {
        const int leading1 = q * szx * szy;
        for (int p = 0; p < szy - 1; ++p) {
          const int leading = p * szx + leading1;
          for (int j = 0; j < szx - 1; ++j)
            for (int k = 0; k < local_node_size; ++k, ++element)
              conn[element] = leading + orientation[cut[k]] + j;
        }
      }
    }
  }

private:
  // Index objects, A++ objects
  Index _index[MAX_DIMENSION];
  friend class CompositeGridHelper;
  friend class CompositeGridFunctionHelper;
};
} // namespace ovcg

#if OVCG_COMPILER
#pragma GCC pop_options
#endif

#undef OVCG_COMPILER

#endif // _OVCG_COMMON_INDEX_CPP_INDEX_HPP
