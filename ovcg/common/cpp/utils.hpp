#pragma once

#include "../../debug.hpp"
#include "../../defines.hpp"

#include <algorithm>
#include <cassert>
#include <vector>

namespace ovcg {

#define FLAG_INTERPOLATION -1

// a collection of handy routines
inline void filter_out_unused_points(int n, const int *mask,
                                     int *out) OVCG_NOEXCEPT {
  OVCG_ASSERT(n > 0, "Invalid number of nodes");
  int count = 0;
  for (int i = 0; i < n; ++i)
    if (mask[i] > 0 || mask[i] == FLAG_INTERPOLATION)
      out[count++] = i;

  assert(std::is_sorted(out, out + count));
}

// filter out unused
inline void filter_out_unused(int n, const int *mask, int elem, int nodes,
                              const int *conn, int *out,
                              std::vector<int> &new_conn) OVCG_NOEXCEPT {
  OVCG_ASSERT(n > 0, "Invalid number of nodes");
  OVCG_ASSERT(elem > 0, "Invalid number of elements");
  OVCG_ASSERT(nodes == 2 || nodes == 4 || nodes == 8, "Unknown element type");

  int count = 0;
  for (int i = 0; i < n; ++i)
    if (mask[i] > 0 || mask[i] == FLAG_INTERPOLATION)
      out[count++] = i;

  assert(std::is_sorted(out, out + count));

  // create a shift map
  std::vector<int> shifts(n, 0);
  for (int i = 0; i < count; ++i)
    shifts[out[i]] = out[i] - i; // sorted

  // create buffer for storing filtered points
  std::vector<int> fpts(n - count);
  count = 0;
  for (int i = 0; i < n; ++i)
    if (mask[i] <= 0 && mask[i] != FLAG_INTERPOLATION)
      fpts[count++] = i;

  assert(std::is_sorted(fpts.begin(), fpts.end()));

  // note that fpts are in sorted order

  // create filter tag, and reset count
  std::vector<bool> filter(elem, false);
  count            = 0;
  const int *conn_ = conn;
  for (int i = 0; i < elem; ++i) {
    for (int j = 0; j < nodes; ++j) {
      std::vector<int>::const_iterator gatcha =
          std::lower_bound(fpts.begin(), fpts.end(), conn_[j]);
      const bool contained = gatcha != fpts.end() && *gatcha <= conn_[j];
      if (contained) {
        filter[i] = true;
        break;
      }
    }
    conn_ += nodes;
    if (!filter[i])
      ++count;
  }

  new_conn.resize(nodes * count);
  std::vector<int>::iterator it = new_conn.begin();
  conn_                         = conn;
  for (int i = 0; i < elem; ++i) {
    if (!filter[i]) {
      for (int j = 0; j < nodes; ++j)
        it[j] = conn_[j] - shifts[conn_[j]];
      it += nodes;
    }
    conn_ += nodes;
  }
}

// flip the normal direction
inline void flip_interface_normals(int elem, int nodes,
                                   int *conn) OVCG_NOEXCEPT {
  // since our interface connectivity is constructed purely based on
  // the structure grids, in general, the interface normals may not be
  // consistent from face to face.
  OVCG_ASSERT(elem > 0, "Invalid number of elements");
  OVCG_ASSERT(nodes == 2 || nodes == 4, "Surface only");
  const static int flip[2][4] = {{1, 0}, {0, 3, 2, 1}};
  int              buff[4];
  const int        d = nodes / 2 - 1;
  for (int i = 0; i < elem; ++i) {
    for (int j = 0; j < nodes; ++j)
      buff[j] = conn[flip[d][j]];
    for (int j = 0; j < nodes; ++j)
      conn[j] = buff[j];
    conn += nodes;
  }
}
}
