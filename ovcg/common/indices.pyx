"""INDICES source file

Internally this instance contains three A++ Index objects

For the orientations in connectivity tables, we follow the CGNS standard
"""

from ovcg.common cimport common
from cython.operator cimport dereference as deref
import cython
import numpy as np
cimport numpy as np
from libcpp cimport bool


cdef class Indices:
    """Indices source definations"""

    def __cinit__(self, other=None):
        """Constructor"""
        self._inst = new common.IndexBox()
        if other is not None:
            self.assign(<Indices> other)


    def __del__(self):
        """Destructor"""
        del self._inst

    def first(self, int axis):
        """Get first index in axis direction"""
        return self._inst.first(<int> axis)

    def last(self, int axis):
        """Get last index in axis direction"""
        return self._inst.last(<int> axis)

    def increment(self, int axis):
        """Get the index increment in axis direction"""
        return self._inst.increment(<int> axis)

    def size(self, int axis):
        """Get the index size count in axis direction"""
        return self._inst.size(<int>axis)

    def get_nodes_number(self):
        """Get the total node counts"""
        return self._inst.get_nodes_number()

    def assign(self, Indices other):
        """Assign to external one"""
        self._inst.assign(deref(other._inst))

    def set(self, int axis, int first, int last, inc=1):
        """Set an index range by user define

        MATLAB syntax: (first:inc:last)
        """
        cdef int increment = <int> inc
        self._inst.set(<int> axis, <int> first, <int> last, increment)

    def copy(self):
        """Make a copy"""
        cdef Indices i = Indices(self)
        return i

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def generate_surface_connectivity(self, int dim, q1=True):
        """Generate a connectivity table for surface

        Since we assume structured FD grids, we can simply generate
        connectivity table just based on x, y, z index range.

        [1] dim: dimension
        [2] q1: flag for Q1 (default) or P1
        """
        cdef int n = self._inst.get_nodes_number()
        cdef bool use_quad = <bool> q1
        cdef np.ndarray[np.int32_t, ndim=2] conn
        sz_x = self.size(0) - 1
        sz_y = self.size(1) - 1
        sz_z = self.size(2) - 1
        nn = 1
        if dim == 1:
            elem = 1
        elif dim == 2:
            nn = n - 1
            elem = 2
        elif dim == 3:
            if sz_x != 0:
                nn *= sz_x
            if sz_y != 0:
                nn *= sz_y
            if sz_z != 0:
                nn *= sz_z
            if q1:
                elem = 4
            else:
                elem = 3
                nn *= 2
        conn = np.zeros(shape=(nn, elem), dtype=np.int32)
        self._inst.generate_surface_connectivity(
            <int> dim, <int *> conn.data, use_quad)
        return conn

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def generate_volume_connectivity(self, int dim, q1=True, six_cuts=True):
        """Generate a connectivity table for volume

        Since we assume structured FD grids, we can simply generate
        connectivity table just based on x, y, z index range.

        [1] dim: dimension
        [2] q1: flag for Q1 (default) or P1
        [3] six_cuts: if 3D, and q1 is False, then this options is to
            indicate whether or not to cut Q1 into 6 (default) or 5
            tetras
        """
        cdef int n = self._inst.get_nodes_number()
        cdef bool use_q1 = <bool> q1
        cdef bool sixc = <bool> six_cuts
        cdef np.ndarray[np.int32_t, ndim=2] conn
        sz_x = self.size(0) - 1
        sz_y = self.size(1) - 1
        sz_z = self.size(2) - 1
        nn = 1
        if sz_x != 0:
            nn *= sz_x
        if sz_y != 0:
            nn *= sz_y
        if sz_z != 0:
            nn *= sz_z
        if dim == 1:
            elem = 2
        elif dim == 2:
            if q1:
                elem = 4
            else:
                elem = 3
                nn *= 2
        elif dim == 3:
            if q1:
                elem = 8
            else:
                elem = 4
                if six_cuts:
                    nn *= 6
                else:
                    nn *= 5
        conn = np.zeros(shape=(nn, elem), dtype=np.int32)
        self._inst.generate_volume_connectivity(
            <int> dim, <int *> conn.data, use_q1, sixc)
        return conn
