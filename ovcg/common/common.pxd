"""COMMON header file"""

from libcpp.string cimport string as std_string
from libcpp cimport bool
from libcpp.vector cimport vector as std_vector


cdef extern from 'ovcg/common/cpp/IndexBox.hpp' namespace 'ovcg':

    cdef cppclass IndexBox:
        IndexBox()
        IndexBox(const IndexBox &other)
        int first(int axis)
        int last(int axis)
        int increment(int axis)
        int size(int axis)
        int get_nodes_number()
        void assign(const IndexBox & other)
        void set(int axis, int first, int last, int inc)
        void generate_surface_connectivity(int dim, int *conn, bool quad)
        void generate_volume_connectivity(
            int dim, int *conn, bool quad, bool six_cuts)


cdef extern from 'ovcg/common/cpp/CompositeGrid.hpp' namespace 'ovcg':

    cdef cppclass CompositeGridHelper:

        # some static methods, may be removed in future release
        @staticmethod
        void initialize_overture()

        @staticmethod
        void finalize_overture()

        CompositeGridHelper()
        void read_hdf(const std_string &filename)
        int get_dimensions()
        int get_gridpoint_number()
        int get_domain_number()
        int get_grid_number()
        int get_component_grid_number()
        int get_multigrid_level_number()
        std_string get_domain_name(int domain)

        # get methods
        void get_indices(int grid, IndexBox &i, bool ghost) except +
        void get_boundary_indices(
            int grid, int side, int axis, IndexBox &i) except +
        void get_ghost_indices(
            int grid, int side, int axis,
            IndexBox & i, int ghost_line) except +
        void get_grid_tags(int *i)
        void get_domain_tags(int *i)
        void get_domain(int d, CompositeGridHelper &cg)

        # for overlap and boundaries
        void get_mask(int grid, const IndexBox &i, int *out)
        void get_interpolation_points(int *out)
        bool may_interpolate(int from_grid, int to_grid, int mlvl)
        void get_interpolation_grids(int *list, int to_grid, int mlvl)
        int get_boundary_share_flag(int grid, int side, int axis)
        int get_boundary_condition_flag(int grid, int side, int axis)
        bool may_cut_holes(int cutting, int cut)
        void get_cutting_grids(int *list, int cut)

        void _retrieve(CompositeGridHelper &cg)
        void _retrieve_level(CompositeGridHelper &cg, int level)


cdef extern from 'ovcg/common/cpp/GeometryData.hpp' namespace 'ovcg':

    cdef cppclass GeometryData:

        GeometryData()
        GeometryData(int grid, CompositeGridHelper &cg)

        void reference(int grid, CompositeGridHelper &cg)
        int get_dimensions()

        void get_vertices(void *out, const IndexBox &i) except +
        void get_vertex_derivative(
            void *out, const IndexBox &i, bool inverse) except +
        void get_vertex_jacobian(void *out, const IndexBox &i) except +
        void get_boundary_normal(
            int side, int axis, void *out, const IndexBox &i) except +
        void get_bounding_box(void *out) except +

        void destroy_field(int f) except +


cdef extern from 'ovcg/common/cpp/CompositeGridFunction.hpp' namespace 'ovcg':

    cdef cppclass CompositeGridFunctionHelper:

        CompositeGridFunctionHelper()
        CompositeGridFunctionHelper(CompositeGridHelper *cg)
        CompositeGridFunctionHelper(CompositeGridHelper *cg, int rank)

        bool has_operators()
        bool has_composite_grids()
        bool has_interpolant()
        int get_dimensions()
        int get_operator_order_of_accuracy()
        int get_grid_number()
        int get_component_grid_number()
        int get_rank()
        int get_component_number_by_name(const std_string &name)
        std_string get_component_name(int comp)
        CompositeGridHelper &get_grids()

        void set_composite_grids(CompositeGridHelper &cg)
        void set_component_name(const std_string &name, int comp)
        void set_operator_order_of_accuracy(int order)
        void interpolate()

        void get_values(int grid, const IndexBox &i, void *out, int comp)
        void get_component_values(int grid, const std_string &name,
                                  const IndexBox &i, void *out)
        void get_grad(int grid, const IndexBox &i, void *out, int comp)
        void get_component_grad(
            int grid, const std_string &name, const IndexBox &i, void *out)


cdef extern from 'ovcg/common/cpp/utils.hpp' namespace 'ovcg':

    void filter_out_unused(
        int n, const int *mask, int elem, int nodes, const int *conn, int *out,
        std_vector[int] &new_conn)
    void flip_interface_normals(int elem, int nodes, int *conn)
    void filter_out_unused_points(int n, const int *mask, int *out)
