"""GRIDS_FUNCTION source file"""

from libcpp cimport bool
from cython.operator cimport dereference as deref
from libcpp.string cimport string as std_string
from .indices cimport Indices
from .grids cimport Grids
import numpy as np
cimport numpy as np
import cython
from ovcg.common cimport common

np.import_array()


cdef class GridsFunction:
    """Grids Function Space

    This class represents the Overture function space class,
    which is the critial component for retrieving function
    values, and modifying boundary conditions
    """

    def __cinit__(self, cg=None, rank=None):
        """Constructor"""
        cdef Grids bar = Grids()
        cdef common.CompositeGridHelper *foo
        cdef int rk
        if cg is not None:
            bar = <Grids> cg
            foo = <common.CompositeGridHelper *> bar._inst
            if rank is not None:
                rk = <int> rank
                self._inst = new common.CompositeGridFunctionHelper(foo, rk)
            else:
                self._inst = new common.CompositeGridFunctionHelper(foo)
        else:
            self._inst = new common.CompositeGridFunctionHelper()

    def __del__(self):
        """Destructor"""
        del self._inst

    def has_operators(self):
        """Check if this function has an underlying operator"""
        return self._inst.has_operators()

    def has_composite_grids(self):
        """Check if this function has an underlying cg"""
        return self._inst.has_composite_grids()

    def has_interpolant(self):
        """Check if this function has an underlying interpolant

        Note that interpolant is for interpolating data associated
        with overset regions
        """
        return self._inst.has_interpolant()

    def get_dimensions(self):
        """Get geometry dimension"""
        return self._inst.get_dimensions()

    def get_operator_order_of_accuracy(self):
        """Get the underlying order of accuracy of the operator"""
        return self._inst.get_operator_order_of_accuracy()

    def get_grid_number(self):
        """Get the grids in the underlying cg"""
        return self._inst.get_grid_number()

    def get_component_grid_number(self):
        """Get the component grid number"""
        return self._inst.get_component_grid_number()

    def get_rank(self):
        """Get the value rank"""
        return self._inst.get_rank()

    def get_component_number_by_name(self, str name):
        """Get the component name

        Note that Overture manage field data into different component,
        for instance, displacement in 3D has 3 components.
        """
        cdef std_string nm = <std_string> name.encode('UTF-8')
        return self._inst.get_component_number_by_name(nm)

    def get_component_name(self, comp=-1):
        """Get the compenent name

        -1 means the master name of the function.
        """
        cdef std_string name = self._inst.get_component_name(<int> comp)
        return name.decode('UTF-8')

    def get_grids(self):
        """Get the underlying grids"""
        cdef Grids cg = Grids()
        cdef common.CompositeGridHelper *inst = \
            <common.CompositeGridHelper *> &self._inst.get_grids()
        inst._retrieve(deref(<common.CompositeGridHelper *> cg._inst))
        return cg

    def set_composite_grids(self, Grids cg):
        """Set a grids to this function"""
        self._inst.set_composite_grids(
            deref(<common.CompositeGridHelper *> cg._inst))

    def set_component_name(self, str name, comp=-1):
        """Set name to component comp

        -1 means the master name of the function.
        """
        cdef std_string nm = <std_string> name.encode('UTF-8')
        self._inst.set_component_name(nm, <int> comp)

    def set_operator_order_of_accuracy(self, int order):
        """Set the order of accuracy

        Note that this depends on the cg, whether or not it supports
        4-th order. (I think) only 2/4 orders are supported.
        """
        self._inst.set_operator_order_of_accuracy(<int> order)

    def interpolate(self):
        """Do internal interpolation"""
        self._inst.interpolate()

    def get_values(self, int grid, Indices indices, comp=0):
        """Get the value of component comp

        Note that -1 is NOT valid! Overture supports components range from
        0 to 4, so total 5 compenents. Including 3 entries to store geometry
        dimension indices, so Overture supports 8 dimensions for each function.
        """
        cdef int n = indices._inst.get_nodes_number()
        cdef np.ndarray[np.float64_t, ndim=1] v = np.zeros(n, dtype=np.float64)
        self._inst.get_values(
            <int> grid,
            deref(<common.IndexBox *> indices._inst),
            <double *> v.data,
            <int> comp)
        return v

    def get_component_values(self, int grid, str name, Indices indices):
        """Get values by component name"""
        cdef int n = indices._inst.get_nodes_number()
        cdef np.ndarray[np.float64_t, ndim=1] v = np.zeros(n, dtype=np.float64)
        cdef std_string nm = <std_string> name.encode('UTF-8')
        self._inst.get_component_values(
            <int> grid,
            nm,
            deref(<common.IndexBox *> indices._inst),
            <double *> v.data)
        return v

    def get_grad(self, int grid, Indices indices, comp=0):
        """Get the gradient of component comp"""
        cdef int dim = self._inst.get_dimensions()
        cdef int n = indices._inst.get_nodes_number()
        cdef np.ndarray[np.float64_t, ndim=2] v = \
            np.zeros(shape=(n, dim), dtype=np.float64)
        self._inst.get_grad(
            <int> grid,
            deref(<common.IndexBox *> indices._inst),
            <double *> v.data, <int> comp)
        return v

    def get_component_grad(self, int grid, str name, Indices indices):
        """Get gradient by compenent name"""
        cdef int dim = self._inst.get_dimensions()
        cdef int n = indices._inst.get_nodes_number()
        cdef std_string nm = <std_string> name.encode('UTF-8')
        cdef np.ndarray[np.float64_t, ndim=2] v = \
            np.zeros(shape=(n, dim), dtype=np.float64)
        self._inst.get_component_grad(
            <int> grid,
            nm,
            deref(<common.IndexBox *> indices._inst),
            <double *> v.data)
        return v
