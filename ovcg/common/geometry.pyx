"""GEOMETRY source file"""

from libcpp cimport bool
from cython.operator cimport dereference as deref
from .indices cimport Indices
from .grids cimport Grids
import numpy as np
cimport numpy as np
import cython
from ovcg.common cimport common

np.import_array()


cdef class Geometry:
    """Geometry implementations

    Retrieve grid geometry information

    Convert structured FD grids into unstructured fashion. The way
    we loop through vertices are:

        for k in z-axis:
            for j in y-axis:
                for i in x-axis:
                    V(i, j, k)
    """

    def __cinit__(self, grid=None, cg=None):
        """Constructor"""
        self._inst = new common.GeometryData()
        if any([foo is not None for foo in [grid, cg]]):
            assert all([foo is not None for foo in [grid, cg]]), \
                'both grid and cg need to be set up.'
            self.reference(grid, cg)

    def __del__(self):
        """Destructor"""
        del self._inst

    def reference(self, int grid, Grids cg):
        """Refer to a component in cg"""
        self._inst.reference(
            <int> grid, deref(<common.CompositeGridHelper *> cg._inst))

    def get_dimensions(self):
        """Get the dimension"""
        return self._inst.get_dimensions()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_vertices(self, Indices indices):
        """Get the vertices given a set of indices"""
        cdef int dim = self._inst.get_dimensions()
        cdef int n = indices._inst.get_nodes_number()
        cdef np.ndarray[np.float64_t, ndim=2] v = \
            np.zeros(shape=(n, dim), dtype=np.float64)
        self._inst.get_vertices(
            <double *> v.data, deref(<common.IndexBox *> indices._inst))
        return v

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_vertex_derivative(self, Indices indices, inverse=False):
        """Get the (inverse) vertex mapping

        A mapping between computational (r s t) <-> physical (x y z)

        How the output is stored:
            normal version:
                [[x_r, x_s, x_t, y_r, y_s, y_t, z_r, z_s, z_t],...]
            inverse version:
                [[r_x, r_y, r_t, s_x, s_y, s_z, t_x, t_y, t_z],...]
        """
        cdef np.ndarray[np.float64_t, ndim=2] v
        cdef int n = indices._inst.get_nodes_number()
        cdef bool inv = <bool> inverse
        dim = self.get_dimensions()
        if dim == 1:
            sz = 1
        elif dim == 2:
            sz = 4
        else:
            sz = 9
        v = np.zeros(shape=(n, sz), dtype=np.float64)
        self._inst.get_vertex_derivative(
            <double *> v.data, deref(<common.IndexBox *> indices._inst), inv)
        return v

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_vertex_jacobian(self, Indices indices):
        """Get the Jacobian on a given set of structured FD grids"""
        cdef int n = indices._inst.get_nodes_number()
        cdef np.ndarray[np.float64_t, ndim=1] v = \
            np.zeros(n, dtype=np.float64)
        self._inst.get_vertex_jacobian(
            <double *> v.data, deref(<common.IndexBox *> indices._inst))
        return v

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_boundary_normal(self, int side, int axis, Indices indices):
        """Get the boundary normal on vertex

        For wrapping purpose, we require side and axis as well as indices
        information, which the actual Overture does not require this much.
        Users are responsible for the consistency between (side, axis)
        and indices.
        """
        cdef int dim = self._inst.get_dimensions()
        cdef int n = indices._inst.get_nodes_number()
        cdef np.ndarray[np.float64_t, ndim=2] v = \
            np.zeros(shape=(n, dim), dtype=np.float64)
        self._inst.get_boundary_normal(
            <int> side,
            <int> axis,
            <double *> v.data,
            deref(<common.IndexBox *> indices._inst))
        return v

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_bounding_box(self):
        """Get the bounding box of this grid

        Stored in:
            [[x_0, y_0, z_0],
             [x_1, y_1, z_1]]
        """
        cdef int dim = self._inst.get_dimensions()
        cdef np.ndarray[np.float64_t, ndim=2] box = \
            np.zeros(shape=(2, dim), dtype=np.float64)
        self._inst.get_bounding_box(<double *> box.data)
        return box

    def destroy_field(self, int field):
        """Destory a field of data to release memory

        VERTEX
        VERTEX_DERIVATIVE
        INVERSE_VERTEX_DERIVATIVE
        VERTEX_JACOBIAN
        VERTEX_BOUNDARY_NORMAL
        BOUNDING_BOX
        EVERYTHING
        """
        self._inst.destroy_field(<int> field)

    def get(self, int field, **kwargs):
        """Get a data field

        VERTEX
        VERTEX_DERIVATIVE
        INVERSE_VERTEX_DERIVATIVE
        VERTEX_JACOBIAN
        VERTEX_BOUNDARY_NORMAL
        BOUNDING_BOX
        """
        assert field >= 0 and field <= 5, 'Invalid field choice.'
        i = kwargs.get('indices', None)
        s = kwargs.get('side', None)
        a = kwargs.get('axis', None)
        if field != 5:
            assert i is not None, 'Indices must be given.'
        if field == 4:
            assert all([foo is not None for foo in [s, a]]), \
                'side/axis missing.'
        if field == 0:
            return self.get_vertices(i)
        if field == 1:
            return self.get_vertex_derivative(i)
        if field == 2:
            return self.get_vertex_derivative(i, True)
        if field == 3:
            return self.get_vertex_jacobian(i)
        if field == 4:
            return self.get_boundary_normal(s, a, i)
        if field == 5:
            return self.get_bounding_box()
