"""CGINS header"""

from libcpp.string cimport string as std_string
from ovcg.common cimport common
from libcpp.vector cimport vector as std_vector
from libcpp cimport bool


cdef extern from 'Cgins.hpp' namespace 'ovcg':

    cdef cppclass CGins:
        CGins()
        void read_cmd(const std_string &file)
        void init_grid()
        void init_solver(int debug, std_string logname)
        void advance(double t, double dt)
        # void save_solutions(const std_string &file)
        int get_total_steps()
        void finalize_solver()
        common.CompositeGridHelper &get_grids()

        # void init_temperature_interface(int grid, int side, int axis)
        void put_temperature_interface(
            int grid, int side, int axis, const void *v) except +
        void put_heat_flux_interface(
            int grid, int side, int axis, const void *v) except +
        void get_normal_heat_flux_interface(
            int grid, int side, int axis, void *v) except +
        const double &get_thermal_conductivity()
        void put_robin_interface(
            int grid, int side, int axis, const void *t_inf, const void *h) except +

        void get_boundary_data_for_testing(
            int grid, int side, int axis, void *v)
        void get_temperature(int grid, const common.IndexBox &ind, void *out)

        double get_cfl()
        double get_cfl_min()
        double get_cfl_max()
        void set_cfl(double v)
        void set_cfl_min(double v)
        void set_cfl_max(double v)

        int determine_substeps(double t, double dt, double *real_dt)
        void advance_step(double dt)

        void backup_current_solutions()
        void restore_previous_solutions()

        int get_dimension()
        int get_grid_number()
        int get_component_grid_number()
        # int get_multigrid_level_number()
        void get_mask(int grid, int *out)
        void get_mask_interface(int grid, int side, int axis, int *out)
        int get_boundary_flag(int grid, int side, int axis)
        std_string get_grid_name(int grid)
        void get_coordinates(int grid, void *out)
        void get_coordinates_interface(int grid, int side, int axis, void *out)
        void get_bounding_box(int grid, void *out)
        int get_vertex_number(int grid)
        int get_vertex_number_interface(int grid, int side, int axis)

        void get_indices(int grid, common.IndexBox &indices)
        void get_indices_interface(
            int grid, int side, int axis, common.IndexBox &indices)

        void get_pressure(int grid, void *out)
        void get_velocity(int grid, void *out)

        bool get_ambient_temperature(int grid, int side, int axis, void *out)
        bool get_delta_coeffs(int grid, int side, int axis, void *out)
        const double &get_cp()
        const double &get_thermal_diffusive()
        const double &get_density(bool test_flag)


cdef class Cgins:
    cdef CGins *_inst
    cdef std_vector[bool] _ifaces
    cdef std_vector[int] _ifaces_types
    cdef bool _init_grid
    cdef int _io_step
