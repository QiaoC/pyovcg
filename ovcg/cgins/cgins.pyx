"""CGINS source file

This is the Cython source file for the definations
of ovcg::CGins.
"""

from libcpp.string cimport string as std_string
from ovcg.common.grids cimport Grids
from ovcg.common.indices cimport Indices
from ovcg.common cimport common
from cython.operator cimport dereference as deref
from ovcg.cgins cimport cgins
import numpy as np
cimport numpy as np
cimport cython
from libcpp cimport bool
from libcpp.vector cimport vector as std_vector

np.import_array()

# We use bit 0 for no-interface, 1 for temperature, 2 for flux
# 4 for robin, 8 for pseudo robin


cdef inline int _pos(int grid, int side, int axis):
    return grid * 6 + side * 3 + axis


cdef inline bool _is_interface(const std_vector[bool] &faces, int pos):
    return faces[pos]


cdef inline int _itype(const std_vector[int] &itype, int pos):
    return itype[pos]


ctypedef enum InterfaceTypes:
    NO_INTERFACE = 0
    TEMPERATURE = 1
    FLUX = TEMPERATURE << 1
    ROBIN = FLUX << 1
    PSEUDO_ROBIN = ROBIN << 1


cdef class Cgins:
    """Cgins source definations"""

    def __cinit__(self):
        """Constructor, this will initialize a CGins instance"""
        self._inst = new cgins.CGins()
        self._init_grid = <bool> 0
        self._io_step = 0

    def __dealloc__(self):
        """Destructor"""
        del self._inst

    def read_cmd(self, str filename):
        """Read in cmd file"""
        cdef std_string fn = <std_string> filename.encode('UTF-8')
        self._inst.read_cmd(fn)

    def init_grid(self):
        """Initialize the mesh"""
        self._inst.init_grid()
        self._ifaces.resize(
            self._inst.get_component_grid_number() * 6, <bool> 0)
        self._ifaces_types.resize(self._ifaces.size(), InterfaceTypes.NO_INTERFACE)
        self._init_grid = <bool> 1

    def init_solver(self, debug=-1, logname=''):
        """Initialize the solver

        if debug < 0, then use debug option in cmd file,
        otherwise, change the debug option to <debug>
        """
        cdef int dbg = <int> debug
        cdef std_string ln = <std_string> logname.encode('UTF-8')
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        self._inst.init_solver(dbg, ln)

    def advance(self, t, dt):
        """Solve one step to time t, with potential value dt"""
        cdef double tt = <double> t
        cdef double dtt = <double> dt
        self._inst.advance(tt, dtt)

    def get_total_steps(self):
        """Get the overall steps, including subiterations, counts"""
        return self._inst.get_total_steps()

    def finalize_solver(self):
        """Finalize solver"""
        self._inst.finalize_solver()

    def get_grids(self):
        """Get the underlying grids"""
        cdef Grids cg = Grids()
        cdef common.CompositeGridHelper *inst = \
            <common.CompositeGridHelper *> &self._inst.get_grids()
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        inst._retrieve(deref(<common.CompositeGridHelper *> cg._inst))
        return cg

    def init_temperature_interface(self, int grid, int side, int axis):
        """Initialize an interface"""
        cdef int pos = _pos(<int> grid, <int> side, <int> axis)
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self.get_component_grid_number()
        assert not self._ifaces[pos], 'Interface exists'
        self._ifaces[pos] = <bool> 1
        self._ifaces_types[pos] = InterfaceTypes.TEMPERATURE

    def init_flux_interface(self, int grid, int side, int axis):
        """Initailize a heat flux interface"""
        cdef int pos = _pos(<int> grid, <int> side, <int> axis)
        assert grid < self.get_component_grid_number()
        assert not self._ifaces[pos], 'Interface exists'
        self._ifaces[pos] = <bool> 1
        self._ifaces_types[pos] = InterfaceTypes.FLUX

    def init_robin_interface(self, int grid, int side, int axis, pseudo=False):
        """Initailize a heat flux interface"""
        cdef int pos = _pos(<int> grid, <int> side, <int> axis)
        assert grid < self.get_component_grid_number()
        assert not self._ifaces[pos], 'Interface exists'
        self._ifaces[pos] = <bool> 1
        if not pseudo:
            self._ifaces_types[pos] = InterfaceTypes.ROBIN
        else:
            self._ifaces_types[pos] = InterfaceTypes.PSEUDO_ROBIN

    def is_temperature_interface(self, int grid, int side, int axis):
        cdef int pos
        assert grid < self.get_component_grid_number()
        pos = _pos(<int> grid, <int> side, <int> axis)
        return _is_interface(self._ifaces, pos) and \
            (_itype(self._ifaces_types, pos) & InterfaceTypes.TEMPERATURE)

    def is_flux_interface(self, int grid, int side, int axis):
        cdef int pos
        assert grid < self.get_component_grid_number()
        pos = _pos(<int> grid, <int> side, <int> axis)
        return _is_interface(self._ifaces, pos) and \
            (_itype(self._ifaces_types, pos) & InterfaceTypes.FLUX)

    def is_robin_interface(self, int grid, int side, int axis, pseudo=False):
        cdef int pos
        cdef int robin
        assert grid < self.get_component_grid_number()
        pos = _pos(<int> grid, <int> side, <int> axis)
        if pseudo:
            robin = InterfaceTypes.PSEUDO_ROBIN
        else:
            robin = InterfaceTypes.ROBIN
        return _is_interface(self._ifaces, pos) and \
            (_itype(self._ifaces_types, pos) & robin)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_normal_heat_flux_interface(self, grid, side, axis):
        """Get the values on normal component of heat flux

        NOTE that the normal is ourward to fluid interface, so that
        it's innerward to solid interface, be careful about the signs
        """
        cdef np.ndarray[np.float64_t, ndim=1] v
        cdef int n
        assert grid < self._inst.get_component_grid_number()
        n = self._inst.get_vertex_number_interface(
            <int> grid, <int> side, <int> axis)
        v = np.empty(n, dtype=np.float64)
        self._inst.get_normal_heat_flux_interface(
            <int> grid, <int> side, <int> axis, <double *> v.data)
        return v

    def get_thermal_conductivity(self):
        """Get the interface thermal conductivity"""
        return self._inst.get_thermal_conductivity()

    def get_heat_flux_interface(self, grid, side, axis):
        """Get the heat flux for CHT

        Q = -kappa*gradient(T)
        """
        return -self.get_thermal_conductivity() * \
            self.get_normal_heat_flux_interface(grid, side, axis)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def put_temperature_interface(
        self, int grid, int side, int axis,
        np.ndarray[np.float64_t, ndim=1] data not None):
        """Set external temperature to CHT interface"""
        cdef int n = data.size
        cdef int m
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        assert self.is_temperature_interface(grid, side, axis)
        m = self._inst.get_vertex_number_interface(
            <int> grid, <int> side, <int> axis)
        assert n == m, 'Unmatched array size'
        self._inst.put_temperature_interface(
            <int> grid, <int> side, <int> axis, <const double *> data.data)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def put_heat_flux_interface(
        self, int grid, int side, int axis,
        np.ndarray[np.float64_t, ndim=1] data not None):
        """Set external heat flux to CHT interface"""
        cdef int n = data.size
        cdef int m
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        assert self.is_flux_interface(grid, side, axis)
        m = self._inst.get_vertex_number_interface(
            <int> grid, <int> side, <int> axis)
        assert n == m, 'Unmatched array size'
        self._inst.put_heat_flux_interface(
            <int> grid, <int> side, <int> axis, <const double *> data.data)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def put_robin_interface(
        self, int grid, int side, int axis,
        np.ndarray[np.float64_t, ndim=1] t_inf not None,
        np.ndarray[np.float64_t, ndim=1] h_coeff not None,
        pseudo=False):
        """Set the ambient temperature and transfer coefficient

        NOTE that pseudo is not handled for now
        """
        cdef int n = t_inf.size
        cdef int m = h_coeff.size
        assert grid < self._inst.get_component_grid_number()
        assert self.is_robin_interface(grid, side, axis, pseudo=pseudo)
        assert n == m
        assert m == self.get_vertex_number_interface(grid, side, axis)
        self._inst.put_robin_interface(
            <int> grid,
            <int> side,
            <int> axis,
            <const double *> t_inf.data,
            <const double *> h_coeff.data)


    @cython.boundscheck(False)
    @cython.wraparound(False)
    def _get_boundary_data_for_testing(self, grid, side, axis):
        cdef Indices ind = self.get_grids().get_boundary_indices(
            <int> grid, <int> side, <int> axis)
        cdef np.ndarray[np.float64_t, ndim=1] v
        cdef int n = ind.get_nodes_number()
        v = np.empty(n, dtype=np.float64)
        self._inst.get_boundary_data_for_testing(
            <int> grid, <int> side, <int> axis, <double *> v.data)
        return v

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def _get_temperature(self, int grid, Indices indices):
        """Get the temperature from indices range"""
        cdef int n = indices._inst.get_nodes_number()
        cdef np.ndarray[np.float64_t, ndim=1] v = np.empty(n, dtype=np.float64)
        self._inst.get_temperature(
            <int> grid,
            deref(<common.IndexBox *> indices._inst),
            <double *> v.data)
        return v

    @property
    def cfl(self):
        """Get cfl"""
        return self._inst.get_cfl()

    @cfl.setter
    def cfl(self, double v):
        """Set cfl"""
        self._inst.set_cfl(<double> v)

    @property
    def cfl_min(self):
        """Get cfl min"""
        return self._inst.get_cfl_min()

    @cfl_min.setter
    def cfl_min(self, double v):
        """Set cfl min"""
        self._inst.set_cfl_min(<double> v)

    @property
    def cfl_max(self):
        """Get cfl max"""
        return self._inst.get_cfl_max()

    @cfl_max.setter
    def cfl_max(self, double v):
        """Set cfl max"""
        self._inst.set_cfl_max(<double> v)

    # advance a step
    def determine_substeps(self, double t, double dt, comp_dt=False):
        """Compute and estimate number of substeps, and opitionally return dt"""
        cdef double ddt
        cdef int n = self._inst.determine_substeps(<double> t, <double> dt, &ddt)
        if comp_dt:
            return n, ddt
        return n

    def advance_step(self, double dt):
        """Advance the solver for a single step

        This function must be used after compute_substeps
        """
        self._inst.advance_step(<double> dt)

    def determine_timestep(self, t, dt):
        """Compute the real time step"""
        n, dtt = self.determine_substeps(t, dt, comp_dt=True)
        return dtt

    def backup_current_solutions(self):
        """backup current solutions"""
        self._inst.backup_current_solutions()

    def restore_previous_solutions(self):
        """Restore previous solutions"""
        self._inst.restore_previous_solutions()

    def get_dimension(self):
        """Get the geometry dimension"""
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        return self._inst.get_dimension()

    def get_grid_number(self):
        """Get the grid number"""
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        return self._inst.get_grid_number()

    def get_component_grid_number(self):
        """Get the componenet grid number"""
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        return self._inst.get_component_grid_number()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_mask(self, int grid):
        """Get the mask array"""
        cdef int n
        cdef np.ndarray[np.int32_t, ndim=1] mask
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        n = self._inst.get_vertex_number(<int> grid)
        mask = np.empty(n, dtype=np.int32)
        self._inst.get_mask(<int> grid, <int *> mask.data)
        return mask

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_mask_interface(self, int grid, int side, int axis):
        """Get the mask tags on an interface"""
        cdef int n
        cdef np.ndarray[np.int32_t, ndim=1] mask
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        n = self._inst.get_vertex_number_interface(
            <int> grid, <int> side, <int> axis)
        mask = np.empty(n, dtype=np.int32)
        self._inst.get_mask_interface(
            <int> grid, <int> side, <int> axis, <int *> mask.data)
        return mask

    def get_boundary_flag(self, int grid, int side, int axis):
        """Return the tag of a boundary face"""
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        return self._inst.get_boundary_flag(<int> grid, <int> side, <int> axis)

    def get_grid_name(self, int grid):
        """Return the name of the grid, this is the name used in Ogen"""
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        return self._inst.get_grid_name(<int> grid).decode('UTF-8')

    def get_coordinates(self, int grid):
        """Get the coordinates, stored in ndarray of size (n, dim)

        The ordering is:

        for k in z-axis:
            for j in y-axis:
                for i in x-axis:
                    V(i, j, k)
        """
        cdef int dim = self._inst.get_dimension()
        cdef int n
        cdef np.ndarray[np.float64_t, ndim=2] v
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        n = self._inst.get_vertex_number(<int> grid)
        v = np.empty(shape=(n, dim), dtype=np.float64)
        self._inst.get_coordinates(<int> grid, <double *> v.data)
        return v

    def get_coordinates_interface(self, int grid, int side, int axis):
        """Get the coordinates, stered in ndarray of size (n, dim)

        The ordering is:

        for k in z-axis:
            for j in y-axis:
                for i in x-axis:
                    V(i, j, k)
        """
        cdef int dim = self._inst.get_dimension()
        cdef int n
        cdef np.ndarray[np.float64_t, ndim=2] v
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        n = self._inst.get_vertex_number_interface(
            <int> grid, <int> side, <int> axis)
        v = np.empty(shape=(n, dim), dtype=np.float64)
        self._inst.get_coordinates_interface(
            <int> grid, <int> side, <int> axis, <double *> v.data)
        return v

    def get_bounding_box(self, int grid):
        """Get the bounding box of this grid

        Stored in:
            [[x_0, y_0, z_0],
             [x_1, y_1, z_1]]
        """
        cdef int dim = self._inst.get_dimension()
        cdef np.ndarray[np.float64_t, ndim=2] box = \
            np.empty(shape=(2, dim), dtype=np.float64)
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        self._inst.get_bounding_box(<int> grid, <double *> box.data)
        return box

    def get_vertex_number(self, int grid):
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        return self._inst.get_vertex_number(<int> grid)

    def get_vertex_number_interface(self, int grid, int side, int axis):
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        return self._inst.get_vertex_number_interface(
            <int> grid, <int> side, <int> axis)

    def generate_connectivity(self, int grid):
        """Generate a connectivity table for volume

        Since we assume structured FD grids, we can simply generate
        connectivity table just based on x, y, z index range.
        """
        cdef Indices i = Indices()
        cdef int dim = self._inst.get_dimension()
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        self._inst.get_indices(<int> grid, deref(<common.IndexBox *> i._inst))
        return i.generate_volume_connectivity(dim)

    def generate_connectivity_interface(self, int grid, int side, int axis):
        """Generate a connectivity table for surface

        Since we assume structured FD grids, we can simply generate
        connectivity table just based on x, y, z index range.
        """
        cdef Indices i = Indices()
        cdef int dim = self._inst.get_dimension()
        assert self._init_grid, 'FATAL! Grid is not initialized!'
        assert grid < self._inst.get_component_grid_number()
        self._inst.get_indices_interface(
            <int> grid, <int> side, <int> axis,
            deref(<common.IndexBox *> i._inst))
        return i.generate_surface_connectivity(dim)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_temperature_interface(self, int grid, int side, int axis):
        """Get the temperature value along an interface"""
        cdef Indices i = Indices()
        assert grid < self._inst.get_component_grid_number()
        self._inst.get_indices_interface(
            <int> grid, <int> side, <int> axis,
            deref(<common.IndexBox *> i._inst))
        return self._get_temperature(grid, i)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_temperature(self, int grid):
        """Get the temperature field in the solver"""
        cdef Indices i = Indices()
        assert grid < self._inst.get_component_grid_number()
        self._inst.get_indices(<int> grid, deref(<common.IndexBox *> i._inst))
        return self._get_temperature(grid, i)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_pressure(self, int grid):
        """Get the pressure field in the solver"""
        cdef int n = self.get_vertex_number(grid)
        cdef np.ndarray[np.float64_t, ndim=1] p = np.empty(n, dtype=np.float64)
        self._inst.get_pressure(<int> grid, <double *> p.data)
        return p

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_velocity(self, int grid):
        """Get the velocity field in the solver"""
        cdef int n = self.get_vertex_number(grid)
        cdef int d = self._inst.get_dimension()
        cdef np.ndarray[np.float64_t, ndim=2] v = \
            np.empty(shape=(n, d), dtype=np.float64)
        self._inst.get_velocity(<int> grid, <double *> v.data)
        return v

    # methods for Robin setting
    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_ambient_temperature(self, int grid, int side, int axis):
        cdef np.ndarray[np.float64_t, ndim=1] t = \
            np.empty(
                self.get_vertex_number_interface(grid, side, axis),
                dtype=np.float64)
        assert self._inst.get_ambient_temperature(
            <int> grid, <int> side, <int> axis, <double *> t.data), \
                'Interface and inner layer don\'t match!'
        return t

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_delta_coeffs(self, int grid, int side, int axis):
        cdef np.ndarray[np.float64_t, ndim=1] dc = \
            np.empty(
                self.get_vertex_number_interface(grid, side, axis),
                dtype=np.float64)
        assert self._inst.get_delta_coeffs(
            <int> grid, <int> side, <int> axis, <double *> dc.data), \
                'Interface and inner layer don\'t match!'
        return dc

    def get_heat_transfer_coeff(self, int grid, int side, int axis):
        return self.get_thermal_conductivity() * \
            self.get_delta_coeffs(grid, side, axis)

    def get_cp(self):
        return self._inst.get_cp()

    def get_thermal_diffusive(self):
        return self._inst.get_thermal_diffusive()

    def get_density(self, test_flag=True):
        cdef bool flag = <bool> test_flag
        return self._inst.get_density(flag)
