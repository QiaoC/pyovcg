#ifndef _OVCG_CGINS_HPP
#define _OVCG_CGINS_HPP 1

#include "../BaseSolver.hpp"

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <vector>

#define DIRICHET_NEUMANN_TAG "externalHeatValues"
#define ROBIN_TAG "externalRobinCoeffs"

namespace ovcg {

// a wrapper of Cgins with reading in command files and holding a piece of mesh

class CGins : public BaseSolver<CGins, OVCG_CGINS> {
public:
  const static std::string name;

  // Constructor
  explicit CGins() OVCG_NOEXCEPT : BaseSolver<CGins, OVCG_CGINS>(),
                                   _step(0),
                                   _init(false) {
    char *cgenv = std::getenv("CG");
    if (cgenv) {
      aString cmd = aString("use lib \"" + aString(cgenv) +
                            "/common/src\"; use CgUtilities;");
      _ps->parseAnswer(cmd);
      cmd = aString("use lib \"" + aString(cgenv) + "/ins/src\"; use CgINS;");
      _ps->parseAnswer(cmd);
    }
  }

  ~CGins() OVCG_NOEXCEPT {
    if (interpolant->decrementReferenceCount() == 0)
      delete interpolant;
  }

  // initialize solver
  void init_solver(int debug = -1, std::string logname = "") OVCG_NOEXCEPT {
    if (!_init) {
      interpolant = new Interpolant(_cg._cg);
      interpolant->incrementReferenceCount();
      interpolant->setImplicitInterpolationMethod(
          Interpolant::iterateToInterpolate);
      _init_solver(debug, logname);
    } else {
      if (logname == "")
        logname = name;
      _solver->parameters.openLogFiles(logname);
      _solver->setParametersInteractively();

      // default = -1, then use option from cmd file
      if (debug >= 0)
        _solver->parameters.dbase.template get<int>("debug") = debug;

      // configure show time at beginning
      Ogshow *show = _solver->parameters.dbase.template get<Ogshow *>("show");
      if (show) {
        _show_file    = std::string(show->getShowFileName());
        _show_changed = false;
      }
    }
    // most code is gotten from common advance
    if (Parameters::checkForFloatingPointErrors) {
      _solver->checkSolution(_solver->gf[_solver->current].u,
                             "advance:start (gf[current])");
    }

    if (!_solver->parameters.dbase.get<DataBase>("modelData")
             .has_key("initializeAdvance"))
      _solver->parameters.dbase.get<DataBase>("modelData")
          .put<int>("initializeAdvance", true);

    // implicit method
    implicitMethod = _solver->parameters.dbase.get<Parameters::ImplicitMethod>(
        "implicitMethod");

    // cfl
    cfl    = _solver->parameters.dbase.get<real>("cfl");
    cflMin = _solver->parameters.dbase.get<real>("cflMin");
    cflMax = _solver->parameters.dbase.get<real>("cflMax");

    _solver->numberOfStepsTaken = max(0, _solver->numberOfStepsTaken);

    _solver->parameters.dbase.get<real>("nextTimeToPrint") = 1e20;

    _t = 0.0;

    // // hard code for testing
    // MappedGrid &            mg = _cg._cg[3];
    // const int               tc = _solver->parameters.dbase.get<int>("tc");
    // realMappedGridFunction &u  = _solver->gf[_solver->current].u[3];
    //
    // Index i[3];
    // // get boundary index
    // getBoundaryIndex(mg.dimension(), 0, 1, i[0], i[1], i[2]);
    // u(i[0], i[1], i[2], tc) = 300.0;
  }

  // advance solver
  void advance(double t, double dt) OVCG_NOEXCEPT {
    // set time
    OVCG_ASSERT(t > _t, "Invalid time point.");
    _assign_dummy_final_time(t);

    // get time method
    Parameters::TimeSteppingMethod &timeSteppingMethod =
        _solver->parameters.dbase.get<Parameters::TimeSteppingMethod>(
            "timeSteppingMethod");

    int &init = _solver->parameters.dbase.get<DataBase>("modelData")
                    .get<int>("initializeAdvance");

    int noss = 1;

    // configure solver steps
    if (timeSteppingMethod != Parameters::steadyStateNewton &&
        timeSteppingMethod != Parameters::implicit &&
        timeSteppingMethod != Parameters::implicitAllSpeed &&
        timeSteppingMethod != Parameters::rKutta) {
      real dtNew = _solver->getTimeStep(_solver->gf[_solver->current]);
      dtNew      = std::min(dt, dtNew);
      _solver->computeNumberOfStepsAndAdjustTheTimeStep(_t, t, t, noss, dtNew);
      _solver->dt = dtNew;
    } else if (timeSteppingMethod == Parameters::rKutta) {
      _solver->parameters.setGridIsImplicit(-1, false);
      _solver->dt =
          std::min(_solver->getTimeStep(_solver->gf[_solver->current]), dt);
    } else if (timeSteppingMethod == Parameters::implicit) {
      // first compute what the explicit time step would be
      const Parameters::TimeSteppingMethod timeSteppingMethodSaved =
          timeSteppingMethod;
      timeSteppingMethod = Parameters::adamsPredictorCorrector2;

      real dtExplicit    = _solver->getTimeStep(_solver->gf[_solver->current]);
      timeSteppingMethod = timeSteppingMethodSaved; // reset

      // now compute the implicit time step
      real dtNew = _solver->getTimeStep(_solver->gf[_solver->current]);
      _solver->computeNumberOfStepsAndAdjustTheTimeStep(_t, t, t, noss, dtNew);

      // --- only change the time step if we exceed the cfl limit or we could
      // increase the time step substantially --
      real ratio = _solver->dt / dtNew;
      if (_step == 0 ||
          implicitMethod == Parameters::approximateFactorization ||
          ratio < cflMin / cfl || ratio > cflMax / cfl) {
        _solver->dt = dtNew;
      } else
        _solver->computeNumberOfStepsAndAdjustTheTimeStep(_t, t, t, noss,
                                                          _solver->dt, false);
    }

    _noss = noss;

    // update time step
    _solver->updateForNewTimeStep(_solver->gf[_solver->current], _solver->dt);
    _solver->parameters.dbase.get<real>("dt") = _solver->dt;

    // advancing
    if (timeSteppingMethod == Parameters::forwardEuler) {
      if (_solver->parameters.dbase.get<int>("useNewAdvanceStepsVersions")) {
        _solver->advanceForwardEulerNew(_t, _solver->dt, noss, init, _step);
        _step += noss;
        _solver->numberOfStepsTaken += noss;
      } else {
        for (int i = 0; i < noss; ++i) {
          const int next = (_solver->current + 1) % 2;
          _solver->eulerStep(_t, _t, _t + _solver->dt, _solver->dt,
                             _solver->gf[_solver->current],
                             _solver->gf[_solver->current], _solver->gf[next],
                             _solver->fn[0], _solver->fn[0], i, noss);
          _t += _solver->dt;
          ++_step;
          ++_solver->numberOfStepsTaken;
          _solver->current = next;
          _solver->output(_solver->gf[_solver->current], _step);
          if ((_solver->numberOfStepsTaken - 1) %
                  _solver->parameters.dbase.get<int>(
                      "frequencyToSaveSequenceInfo") ==
              0)
            if (!_solver->parameters.isAdaptiveGridProblem())
              _solver->saveSequenceInfo(_t, _solver->fn[0]);
        }
      }
    } else if (timeSteppingMethod == Parameters::midPoint) {
      _solver->advanceMidPoint(_t, _solver->dt, noss, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
      _solver->parameters.dbase.get<int>("globalStepNumber") += noss;
    } else if (timeSteppingMethod == Parameters::adi) {
      _solver->advanceADI(_t, _solver->dt, noss, init, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
      _solver->parameters.dbase.get<int>("globalStepNumber") += noss;
    } else if (timeSteppingMethod == Parameters::adamsBashforth2 ||
               timeSteppingMethod == Parameters::adamsPredictorCorrector2 ||
               timeSteppingMethod == Parameters::adamsPredictorCorrector4) {
      if (_solver->parameters.dbase.get<int>("useNewAdvanceStepsVersions"))
        _solver->advanceAdamsPredictorCorrectorNew(_t, _solver->dt, noss, init,
                                                   _step);
      else
        _solver->advanceAdamsPredictorCorrector(_t, _solver->dt, noss, init,
                                                _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
    } else if (timeSteppingMethod ==
               Parameters::variableTimeStepAdamsPredictorCorrector) {
      _solver->advanceVariableTimeStepAdamsPredictorCorrector(
          _t, _solver->dt, noss, init, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
      _solver->parameters.dbase.get<int>("globalStepNumber") += noss;
    } else if (timeSteppingMethod == Parameters::laxFriedrichs) {
      Overture::abort("ERROR -- fix this Bill!");
    } else if (timeSteppingMethod == Parameters::steadyStateRungeKutta) {
      _solver->advanceSteadyStateRungeKutta(_t, _solver->dt, noss, init, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
    } else if (timeSteppingMethod == Parameters::implicit) {
      if (implicitMethod == Parameters::trapezoidal)
        _solver->advanceTrapezoidal(_t, _solver->dt, noss, init, _step);
      else {
        if (_solver->parameters.dbase.get<int>("useNewAdvanceStepsVersions") ||
            implicitMethod == Parameters::approximateFactorization ||
            implicitMethod == Parameters::backwardDifferentiationFormula) {
          _solver->advanceImplicitMultiStepNew(_t, _solver->dt, noss, init,
                                               _step);
        } else
          _solver->advanceImplicitMultiStep(_t, _solver->dt, noss, init, _step);
      }
      _step += noss;
      _solver->numberOfStepsTaken += noss;
    } else if (timeSteppingMethod == Parameters::steadyStateNewton) {
      _solver->advanceNewton(_t, _solver->dt, noss, init, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
    } else if (timeSteppingMethod == Parameters::nonMethodOfLines) {
      Overture::abort("ERROR -- fix this Bill!");
    } else if (timeSteppingMethod == Parameters::implicitAllSpeed) {
      _solver->allSpeedImplicitTimeStep(_solver->gf[_solver->current], _t,
                                        _solver->dt, noss, t);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
      _solver->parameters.dbase.get<int>("globalStepNumber") += noss;
    } else if (timeSteppingMethod ==
               Parameters::secondOrderSystemTimeStepping) {
      _solver->advanceSecondOrderSystem(_t, _solver->dt, noss, init, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
    } else
      Overture::abort("error");

    // finish
    _t = t;
  }

  // init interface, this will allocate memory for Cgins user boundary data
  void init_temperature_interface(int grid, int side, int axis) OVCG_NOEXCEPT {
    OVCG_ASSERT(grid < _cg.get_component_grid_number(), "Outof range");
    if (!_init) {
      interpolant = new Interpolant(_cg._cg);
      interpolant->incrementReferenceCount();
      interpolant->setImplicitInterpolationMethod(
          Interpolant::iterateToInterpolate);
      bool    plotOption = false;
      Ogshow *show       = nullptr;
      _solver            = new Cgins(_cg._cg, _ps, show, plotOption);
      _solver->setNameOfGridFile(aString(_grid_file));
      _init = true;
    }
    // MappedGrid &mg = _cg._cg[grid];
    // Index i[3];
    // getBoundaryIndex(mg.gridIndexRange(), side, axis, i[0], i[1], i[2]);
    // const int sz = i[0].length() * i[1].length() * i[2].length();
    //
    // RealArray values(sz);
    //
    // // resize the base array in boundary data important!
    // values = 0.0;
    // _solver->parameters.setUserBoundaryConditionParameters(side, axis, grid,
    //                                                        values);
    // MappedGrid &mg = _cg._cg[grid];
    // Index i[3];
    // // get boundary index
    // getBoundaryIndex(mg.gridIndexRange(), side, axis, i[0], i[1], i[2]);
    // const int sz = i[0].length() * i[1].length() * i[2].length();
    // if (bd[side][axis][grid].getLength(0) != sz)
    //   bd[side][axis][grid].resize(sz);

    // // get the internal array
    // RealArray &values = _solver->parameters.dbase.get<RealArray>(
    //     "userBoundaryConditionParameters");
    // int numberOfGrids =
    //     _solver->parameters.dbase.get<RealArray>("bcParameters").getLength(3);
    // values.resize(sz, 2, 3, numberOfGrids);

    // // testing  the following steps is to explicitly set normal coefficient
    // // in mixed boundary condition to be zero
    // int numberOfComponents =
    //     _solver->parameters.dbase.get<int>("numberOfComponents");
    // const int tc = _solver->parameters.dbase.get<int>("tc");
    // RealArray &bcData = _solver->parameters.dbase.get<RealArray>("bcData");
    // bcData(tc + 2 * numberOfComponents, side, axis, grid) = 0.0;
  }

  // put in interface temperature values
  void put_temperature_interface(int grid, int side, int axis, const void *v) {
    OVCG_ASSERT(grid < _cg.get_component_grid_number(), "Outof range");
    const aString userDefinedBoundaryValueName =
        sPrintF("userBV_G%i_S%i_A%i", grid, side, axis);
    if (!_solver->parameters.dbase.has_key(userDefinedBoundaryValueName))
      throw std::runtime_error("This interface has not been registered");
    const aString &userDefinedBoundaryValue =
        _solver->parameters.dbase.get<aString>(userDefinedBoundaryValueName);
    if (userDefinedBoundaryValue != DIRICHET_NEUMANN_TAG)
      throw std::runtime_error("Wrong Interface!");

    MappedGrid &mg = _cg._cg[grid];
    Index       i[3];
    // get boundary index
    getBoundaryIndex(mg.gridIndexRange(), side, axis, i[0], i[1], i[2]);
    const int sz = i[0].length() * i[1].length() * i[2].length();

    RealArray values(sz);

    const real *vv = reinterpret_cast<const real *>(v);

    for (int i = 0; i < sz; ++i)
      values(i) = vv[i];

    _solver->parameters.setUserBoundaryConditionParameters(side, axis, grid,
                                                           values);
  }

  // put in interface heat flux values, make sure the interface is neumann
  void put_heat_flux_interface(int grid, int side, int axis, const void *v) {
    OVCG_ASSERT(grid < _cg.get_component_grid_number(), "Outof range");
    const aString userDefinedBoundaryValueName =
        sPrintF("userBV_G%i_S%i_A%i", grid, side, axis);
    if (!_solver->parameters.dbase.has_key(userDefinedBoundaryValueName))
      throw std::runtime_error("This interface has not been registered");
    const aString &userDefinedBoundaryValue =
        _solver->parameters.dbase.get<aString>(userDefinedBoundaryValueName);
    if (userDefinedBoundaryValue != DIRICHET_NEUMANN_TAG)
      throw std::runtime_error("Wrong Interface!");

    MappedGrid &mg = _cg._cg[grid];
    Index       i[3];
    // get boundary index
    getBoundaryIndex(mg.gridIndexRange(), side, axis, i[0], i[1], i[2]);
    const int sz = i[0].length() * i[1].length() * i[2].length();

    RealArray values(sz);

    const real *vv = reinterpret_cast<const real *>(v);

    const double &kappa = get_thermal_conductivity();
    OVCG_ASSERT(kappa > 0.0, "Invalid conductivity coefficient");

    for (int i = 0; i < sz; ++i)
      values(i) = vv[i] / kappa;

    _solver->parameters.setUserBoundaryConditionParameters(side, axis, grid,
                                                           values);
  }

  // put the ambient temp heat transfer coefficient from the other side
  void put_robin_interface(int grid, int side, int axis, const void *t_inf,
                           const void *h) {
    OVCG_ASSERT(grid < _cg.get_component_grid_number(), "Outof range");
    const aString userDefinedBoundaryValueName =
        sPrintF("userBV_G%i_S%i_A%i", grid, side, axis);
    if (!_solver->parameters.dbase.has_key(userDefinedBoundaryValueName))
      throw std::runtime_error("This interface has not been registered");
    const aString &userDefinedBoundaryValue =
        _solver->parameters.dbase.get<aString>(userDefinedBoundaryValueName);
    if (userDefinedBoundaryValue != ROBIN_TAG)
      throw std::runtime_error("Wrong Interface!");

    MappedGrid &mg = _cg._cg[grid];
    Index       i[3];
    // get boundary index
    getBoundaryIndex(mg.gridIndexRange(), side, axis, i[0], i[1], i[2]);
    const int sz = i[0].length() * i[1].length() * i[2].length();

    const real *T_inf = reinterpret_cast<const real *>(t_inf);
    const real *H     = reinterpret_cast<const real *>(h);

    // the first half stores ambient temperature, rest for transfer coefficients
    RealArray values(2 * sz);
    for (int i = 0; i < sz; ++i)
      values(i) = T_inf[i];
    for (int i = sz, j = 0; i < 2 * sz; ++i, ++j)
      values(i) = H[j];

    _solver->parameters.setUserBoundaryConditionParameters(side, axis, grid,
                                                           values);
  }

  // this will get the interface heat flux
  // This function is supposed to be called when we have a fully configured
  // Cgins solver. Otherwise, some components may be missing.
  // NOTE that when the flux is tranferred to solid, this is actually
  // flux of INNERWARD normal component!
  void get_normal_heat_flux_interface(int grid, int side, int axis,
                                      void *v) OVCG_NOEXCEPT {
    OVCG_ASSERT(grid < _cg.get_component_grid_number(), "Outof range");
    MappedGrid &mg = _cg._cg[grid];
    const int   tc = _solver->parameters.dbase.get<int>("tc");

    // update boundary normal
    mg.update(MappedGrid::THEvertexBoundaryNormal);
#ifdef USE_PPP
    const realSerialArray &normal = mg.vertexBoundaryNormalArray(side, axis);
#else
    const realSerialArray &normal = mg.vertexBoundaryNormal(side, axis);
#endif

    // we use current, this routine is supposed to be called after advancing
    realMappedGridFunction &u = _solver->gf[_solver->current].u[grid];
#ifdef USE_PPP
    realSerialArray uLocal;
    getLocalArrayWithGhostBoundaries(u, uLocal);
#else
    realSerialArray &      uLocal = u;
#endif

    // get the operator
    MappedGridOperators &op = *(u.getOperators());

    Index i[3];
    // get boundary index
    getBoundaryIndex(mg.gridIndexRange(), side, axis, i[0], i[1], i[2]);
    const int sz = i[0].length() * i[1].length() * i[2].length();

    Range N(tc, tc);

    // u'
    realSerialArray ud(i[0], i[1], i[2], N), flux(i[0], i[1], i[2]);

    // compute the derivative u_x
    op.derivative(MappedGridOperators::xDerivative, uLocal, ud, i[0], i[1],
                  i[2], N);
    flux = normal(i[0], i[1], i[2], 0) * ud;

    // compute the derivative u_y
    op.derivative(MappedGridOperators::yDerivative, uLocal, ud, i[0], i[1],
                  i[2], N);
    flux += normal(i[0], i[1], i[2], 1) * ud;

    if (_cg.get_dimensions() == 3) {
      // compute the derivative u_z
      op.derivative(MappedGridOperators::zDerivative, uLocal, ud, i[0], i[1],
                    i[2], N);
      flux += normal(i[0], i[1], i[2], 2) * ud;
    }

    // cast
    real *vv    = reinterpret_cast<real *>(v);
    int   count = 0;
    for (int k = i[2].getBase(); k <= i[2].getBound(); k += i[0].getStride())
      for (int j = i[1].getBase(); j <= i[1].getBound(); j += i[1].getStride())
        for (int q = i[0].getBase(); q <= i[0].getBound();
             q += i[0].getStride(), ++count)
          vv[count] = flux(q, j, k);
  }

  // get the conductivity coefficient on interface
  const double &get_thermal_conductivity() const OVCG_NOEXCEPT {
    return static_cast<const double &>(
        _solver->parameters.dbase.get<real>("thermalConductivity"));
  }

  // test routines
  // get temperature just signed for testing
  void get_boundary_data_for_testing(int grid, int side, int axis,
                                     void *v) OVCG_NOEXCEPT {
    OVCG_ASSERT(grid < _cg.get_component_grid_number(), "Outof range");
    MappedGrid &mg = _cg._cg[grid];
    const int   tc = _solver->parameters.dbase.get<int>("tc");
    RealArray & bd = _solver->parameters.getBoundaryData(side, axis, grid, mg);
    Index       i[3];
    // get boundary index
    getBoundaryIndex(mg.gridIndexRange(), side, axis, i[0], i[1], i[2]);
    // cast
    real *vv    = reinterpret_cast<real *>(v);
    int   count = 0;
    for (int k = i[2].getBase(); k <= i[2].getBound(); k += i[0].getStride())
      for (int j = i[1].getBase(); j <= i[1].getBound(); j += i[1].getStride())
        for (int q = i[0].getBase(); q <= i[0].getBound();
             q += i[0].getStride(), ++count)
          vv[count] = bd(q, j, k, tc);
  }

  void get_temperature(int grid, const IndexBox &ind, void *out) OVCG_NOEXCEPT {
    OVCG_ASSERT(grid < _cg.get_component_grid_number(), "Outof range");
    realMappedGridFunction &u     = _solver->gf[_solver->current].u[grid];
    const int               tc    = _solver->parameters.dbase.get<int>("tc");
    int                     count = 0;
    // cast
    real *v = reinterpret_cast<real *>(out);
    for (int k = ind.first(2); k <= ind.last(2); ++k)
      for (int j = ind.first(1); j <= ind.last(1); ++j)
        for (int i = ind.first(0); i <= ind.last(0); ++i, ++count)
          v[count] = u(i, j, k, tc);
  }

  void finalize_solver() OVCG_NOEXCEPT {}

  // get cfl
  double get_cfl() const OVCG_NOEXCEPT { return static_cast<double>(cfl); }
  double get_cfl_min() const OVCG_NOEXCEPT {
    return static_cast<double>(cflMin);
  }
  double get_cfl_max() const OVCG_NOEXCEPT {
    return static_cast<double>(cflMax);
  }

  // set cfl
  void set_cfl(double v) OVCG_NOEXCEPT {
    cfl = _solver->parameters.dbase.get<real>("cfl") = v;
  }
  void set_cfl_min(double v) OVCG_NOEXCEPT {
    cflMin = _solver->parameters.dbase.get<real>("cflMin") = v;
  }
  void set_cfl_max(double v) OVCG_NOEXCEPT {
    cflMax = _solver->parameters.dbase.get<real>("cflMax") = v;
  }

  // The following two functions will estimate the substeps and setup
  // the dt for the solver
  int determine_substeps(double t, double dt,
                         double *real_dt = nullptr) OVCG_NOEXCEPT {
    // set time
    if (t - _t <= 0.0)
      return 0;
    OVCG_ASSERT(t > _t, "Invalid time point.");
    _assign_dummy_final_time(t);

    // get time method
    Parameters::TimeSteppingMethod &timeSteppingMethod =
        _solver->parameters.dbase.get<Parameters::TimeSteppingMethod>(
            "timeSteppingMethod");

    int &init = _solver->parameters.dbase.get<DataBase>("modelData")
                    .get<int>("initializeAdvance");

    int noss = 1;

    // configure solver steps
    if (timeSteppingMethod != Parameters::steadyStateNewton &&
        timeSteppingMethod != Parameters::implicit &&
        timeSteppingMethod != Parameters::implicitAllSpeed &&
        timeSteppingMethod != Parameters::rKutta) {
      real dtNew = _solver->getTimeStep(_solver->gf[_solver->current]);
      dtNew      = std::min(dt, dtNew);
      _solver->computeNumberOfStepsAndAdjustTheTimeStep(_t, t, t, noss, dtNew);
      _solver->dt = dtNew;
    } else if (timeSteppingMethod == Parameters::rKutta) {
      _solver->parameters.setGridIsImplicit(-1, false);
      _solver->dt =
          std::min(_solver->getTimeStep(_solver->gf[_solver->current]), dt);
    } else if (timeSteppingMethod == Parameters::implicit) {
      // first compute what the explicit time step would be
      const Parameters::TimeSteppingMethod timeSteppingMethodSaved =
          timeSteppingMethod;
      timeSteppingMethod = Parameters::adamsPredictorCorrector2;

      real dtExplicit    = _solver->getTimeStep(_solver->gf[_solver->current]);
      timeSteppingMethod = timeSteppingMethodSaved; // reset

      // now compute the implicit time step
      real dtNew = _solver->getTimeStep(_solver->gf[_solver->current]);
      _solver->computeNumberOfStepsAndAdjustTheTimeStep(_t, t, t, noss, dtNew);

      // --- only change the time step if we exceed the cfl limit or we could
      // increase the time step substantially --
      real ratio = _solver->dt / dtNew;
      if (_step == 0 ||
          implicitMethod == Parameters::approximateFactorization ||
          ratio < cflMin / cfl || ratio > cflMax / cfl) {
        _solver->dt = dtNew;
      } else
        _solver->computeNumberOfStepsAndAdjustTheTimeStep(_t, t, t, noss,
                                                          _solver->dt, false);
    }

    // update time step
    _solver->updateForNewTimeStep(_solver->gf[_solver->current], _solver->dt);
    _solver->parameters.dbase.get<real>("dt") = _solver->dt;

    if (real_dt)
      *real_dt = _solver->dt;
    _noss = noss;
    return noss;
  }

  // advance a single step, must be called after one knows the substeps
  void advance_step(double dt) OVCG_NOEXCEPT {
    _solver->dt = dt <= 0.0 ? _solver->dt : dt;

    // get time method
    Parameters::TimeSteppingMethod &timeSteppingMethod =
        _solver->parameters.dbase.get<Parameters::TimeSteppingMethod>(
            "timeSteppingMethod");

    int &init = _solver->parameters.dbase.get<DataBase>("modelData")
                    .get<int>("initializeAdvance");

    int noss = 1;

    if (timeSteppingMethod == Parameters::forwardEuler) {
      if (_solver->parameters.dbase.get<int>("useNewAdvanceStepsVersions")) {
        _solver->advanceForwardEulerNew(_t, _solver->dt, noss, init, _step);
        _step += noss;
        _solver->numberOfStepsTaken += noss;
      } else {
        const int next = (_solver->current + 1) % 2;
        _solver->eulerStep(_t, _t, _t + _solver->dt, _solver->dt,
                           _solver->gf[_solver->current],
                           _solver->gf[_solver->current], _solver->gf[next],
                           _solver->fn[0], _solver->fn[0], 0, noss);
        ++_step;
        ++_solver->numberOfStepsTaken;
        _solver->current = next;
        _solver->output(_solver->gf[_solver->current], _step);
        if ((_solver->numberOfStepsTaken - 1) %
                _solver->parameters.dbase.get<int>(
                    "frequencyToSaveSequenceInfo") ==
            0)
          if (!_solver->parameters.isAdaptiveGridProblem())
            _solver->saveSequenceInfo(_t, _solver->fn[0]);
      }
    } else if (timeSteppingMethod == Parameters::midPoint) {
      _solver->advanceMidPoint(_t, _solver->dt, noss, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
      _solver->parameters.dbase.get<int>("globalStepNumber") += noss;
    } else if (timeSteppingMethod == Parameters::adi) {
      _solver->advanceADI(_t, _solver->dt, noss, init, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
      _solver->parameters.dbase.get<int>("globalStepNumber") += noss;
    } else if (timeSteppingMethod == Parameters::adamsBashforth2 ||
               timeSteppingMethod == Parameters::adamsPredictorCorrector2 ||
               timeSteppingMethod == Parameters::adamsPredictorCorrector4) {
      if (_solver->parameters.dbase.get<int>("useNewAdvanceStepsVersions"))
        _solver->advanceAdamsPredictorCorrectorNew(_t, _solver->dt, noss, init,
                                                   _step);
      else
        _solver->advanceAdamsPredictorCorrector(_t, _solver->dt, noss, init,
                                                _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
    } else if (timeSteppingMethod ==
               Parameters::variableTimeStepAdamsPredictorCorrector) {
      _solver->advanceVariableTimeStepAdamsPredictorCorrector(
          _t, _solver->dt, noss, init, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
      _solver->parameters.dbase.get<int>("globalStepNumber") += noss;
    } else if (timeSteppingMethod == Parameters::laxFriedrichs) {
      Overture::abort("ERROR -- fix this Bill!");
    } else if (timeSteppingMethod == Parameters::steadyStateRungeKutta) {
      _solver->advanceSteadyStateRungeKutta(_t, _solver->dt, noss, init, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
    } else if (timeSteppingMethod == Parameters::implicit) {
      if (implicitMethod == Parameters::trapezoidal)
        _solver->advanceTrapezoidal(_t, _solver->dt, noss, init, _step);
      else {
        if (_solver->parameters.dbase.get<int>("useNewAdvanceStepsVersions") ||
            implicitMethod == Parameters::approximateFactorization ||
            implicitMethod == Parameters::backwardDifferentiationFormula) {
          _solver->advanceImplicitMultiStepNew(_t, _solver->dt, noss, init,
                                               _step);
        } else
          _solver->advanceImplicitMultiStep(_t, _solver->dt, noss, init, _step);
      }
      _step += noss;
      _solver->numberOfStepsTaken += noss;
    } else if (timeSteppingMethod == Parameters::steadyStateNewton) {
      _solver->advanceNewton(_t, _solver->dt, noss, init, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
    } else if (timeSteppingMethod == Parameters::nonMethodOfLines) {
      Overture::abort("ERROR -- fix this Bill!");
    } else if (timeSteppingMethod == Parameters::implicitAllSpeed) {
      _solver->allSpeedImplicitTimeStep(_solver->gf[_solver->current], _t,
                                        _solver->dt, noss, _t + _solver->dt);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
      _solver->parameters.dbase.get<int>("globalStepNumber") += noss;
    } else if (timeSteppingMethod ==
               Parameters::secondOrderSystemTimeStepping) {
      _solver->advanceSecondOrderSystem(_t, _solver->dt, noss, init, _step);
      _step += noss;
      _solver->numberOfStepsTaken += noss;
    } else
      Overture::abort("error");

    // finish
    _t += _solver->dt;
  }

  // backup the solution before advancing
  // NOTE that this only backup the solution array, for moving domain problems
  // the velocity of grids is not backed up.
  void backup_current_solutions() OVCG_NOEXCEPT {
    t_bak   = _t;
    sol_bak = _solver->gf[_solver->current].u;
  }

  // restore solutions
  void restore_previous_solutions() OVCG_NOEXCEPT {
    _solver->gf[_solver->current].u = sol_bak;
    _t = _solver->gf[_solver->current].t = t_bak;
  }

private:
  Parameters::ImplicitMethod implicitMethod;
  int                        _step;
  real                       cfl, cflMin, cflMax;
  Interpolant *              interpolant;
  int                        _noss;
  bool                       _init;
  real                       t_bak;
  realCompositeGridFunction  sol_bak;

  // assign dummy final time to the solver
  void _assign_dummy_final_time(double t) OVCG_NOEXCEPT {
    _solver->parameters.dbase.get<real>("tFinal") =
        _solver->parameters.dbase.get<real>("tPrint") = t;
  }

public:
  // interfaces to grids
  int get_dimension() const OVCG_NOEXCEPT { return _cg.get_dimensions(); }
  int get_gridpoint_number() const OVCG_NOEXCEPT {
    return _cg.get_gridpoint_number();
  }
  int get_grid_number() const OVCG_NOEXCEPT { return _cg.get_grid_number(); }
  int get_component_grid_number() const OVCG_NOEXCEPT {
    return _cg.get_component_grid_number();
  }
  int get_multigrid_level_number() const OVCG_NOEXCEPT {
    return _cg.get_multigrid_level_number();
  }

  void get_mask(int grid, int *out) {
    IndexBox i;
    _cg.get_indices(grid, i, false); // no ghost
    _cg.get_mask(grid, i, out);
  }

  void get_mask_interface(int grid, int side, int axis, int *out) {
    IndexBox i;
    _cg.get_boundary_indices(grid, side, axis, i);
    _cg.get_mask(grid, i, out);
  }

  int get_boundary_flag(int grid, int side, int axis) const OVCG_NOEXCEPT {
    return _cg.get_boundary_condition_flag(grid, side, axis);
  }

  std::string get_grid_name(int grid) const OVCG_NOEXCEPT {
    OVCG_ASSERT(grid < _cg.get_component_grid_number(), "Outof range");
    return _cg._cg[grid].getName();
  }

  // interfaces to grid data
  void get_coordinates(int grid, void *out) {
    IndexBox i;
    _cg.get_indices(grid, i, false); // no ghost
    _gd[grid].get_vertices(out, i);
  }

  void get_coordinates_interface(int grid, int side, int axis, void *out) {
    IndexBox i;
    _cg.get_boundary_indices(grid, side, axis, i);
    _gd[grid].get_vertices(out, i);
  }

  void get_bounding_box(int grid, void *out) {
    OVCG_ASSERT(grid < _cg.get_component_grid_number(), "Outof range");
    _gd[grid].get_bounding_box(out);
  }

  int get_vertex_number(int grid) const OVCG_NOEXCEPT {
    OVCG_ASSERT(grid < _cg.get_component_grid_number(), "Outof range");
    IndexBox i;
    _cg.get_indices(grid, i, false); // no ghost
    return i.get_nodes_number();
  }

  int get_vertex_number_interface(int grid, int side,
                                  int axis) const OVCG_NOEXCEPT {
    IndexBox i;
    _cg.get_boundary_indices(grid, side, axis, i);
    return i.get_nodes_number();
  }

  void get_indices(int grid, IndexBox &indices) const OVCG_NOEXCEPT {
    _cg.get_indices(grid, indices, false);
  }

  void get_indices_interface(int grid, int side, int axis,
                             IndexBox &indices) const OVCG_NOEXCEPT {
    _cg.get_boundary_indices(grid, side, axis, indices);
  }

  // get all solutions
  void get_pressure(int grid, void *out) const OVCG_NOEXCEPT {
    IndexBox ind;
    get_indices(grid, ind);
    const realMappedGridFunction &u  = _solver->gf[_solver->current].u[grid];
    const int                     pc = _solver->parameters.dbase.get<int>("pc");
    int                           count = 0;
    real *                        p     = reinterpret_cast<real *>(out);
    for (int k = ind.first(2); k <= ind.last(2); ++k)
      for (int j = ind.first(1); j <= ind.last(1); ++j)
        for (int i = ind.first(0); i <= ind.last(0); ++i, ++count)
          p[count] = u(i, j, k, pc);
  }

  void get_velocity(int grid, void *out) const OVCG_NOEXCEPT {
    IndexBox ind;
    get_indices(grid, ind);
    const realMappedGridFunction &u = _solver->gf[_solver->current].u[grid];
    int                           vs[3];
    vs[0]           = _solver->parameters.dbase.get<int>("uc");
    vs[1]           = _solver->parameters.dbase.get<int>("vc");
    vs[2]           = _solver->parameters.dbase.get<int>("wc");
    const int d     = get_dimension();
    real *    V     = reinterpret_cast<real *>(out);
    int       count = 0;
    for (int k = ind.first(2); k <= ind.last(2); ++k)
      for (int j = ind.first(1); j <= ind.last(1); ++j)
        for (int i = ind.first(0); i <= ind.last(0); ++i)
          for (int dim = 0; dim < d; ++dim, ++count)
            V[count] = u(i, j, k, vs[dim]);
  }

  // methods for robin and pseudo-robin, we take the first line inside the
  // interface.

  // NOTE that we assumes this is for CHT problem for retreving
  // the information of thermal layers. So the grids here are fine. Moreover,
  // we assume that the inner layer have the interface have the same grid
  // types. The return indicates the boundary and inner are matched
  bool get_ambient_temperature(int grid, int side, int axis,
                               void *out) const OVCG_NOEXCEPT {
    IndexBox ind;
    get_indices_interface(grid, side, axis, ind);
    Index             I[3];
    const MappedGrid &mg = _cg._cg[grid];
    getGhostIndex(mg.gridIndexRange(), side, axis, I[0], I[1], I[2], -1);
    for (int i = 0; i < 3; ++i)
      if (ind.size(i) != I[i].length())
        return false;
    const realMappedGridFunction &u = _solver->gf[_solver->current].u[grid];
    const int tc    = _solver->parameters.dbase.get<int>("tc"); // temp
    real *    t     = reinterpret_cast<real *>(out);
    int       count = 0;
    for (int k = I[2].getBase(); k <= I[2].getBound(); k += I[2].getStride())
      for (int j = I[1].getBase(); j <= I[1].getBound(); j += I[1].getStride())
        for (int i = I[0].getBase(); i <= I[0].getBound();
             i += I[0].getStride(), ++count)
          t[count] = u(i, j, k, tc);
    return true;
  }

  // get the delta coefficient, NOTE we assume orthognality between interface
  // and first inner layer
  bool get_delta_coeffs(int grid, int side, int axis, void *out) OVCG_NOEXCEPT {
    IndexBox ind;
    get_indices_interface(grid, side, axis, ind);
    Index       I[3];
    MappedGrid &mg = _cg._cg[grid];
    getGhostIndex(mg.gridIndexRange(), side, axis, I[0], I[1], I[2], -1);
    for (int i = 0; i < 3; ++i)
      if (ind.size(i) != I[i].length())
        return false;

    real *    delta_coeffs = reinterpret_cast<real *>(out);
    const int dim          = get_dimension();
    int       count        = 0;
    mg.update(MappedGrid::THEvertex); // update vertices

    // TODO is the following code correct?
    for (int kb = ind.first(2), ki = I[2].getBase(); kb <= ind.last(2);
         kb += ind.increment(2), ki += I[2].getStride())
      for (int jb = ind.first(1), ji = I[1].getBase(); jb <= ind.last(1);
           jb += ind.increment(1), ji += I[1].getStride())
        for (int ib = ind.first(0), ii = I[0].getBase(); ib <= ind.last(0);
             ib += ind.increment(0), ii += I[0].getStride()) {
          real eu_dis_sq = 0.0;
          for (int d = 0; d < dim; ++d) {
            // TODO basically, is the following always valid?
            const real dis =
                mg.vertex()(ib, jb, kb, d) - mg.vertex()(ii, ji, ki, d);
            eu_dis_sq += dis * dis;
          }
          delta_coeffs[count] = 1.0 / std::sqrt(eu_dis_sq);
          ++count;
        }
    return true;
  }

  // get the specific heat for testing
  const double &get_cp() const OVCG_NOEXCEPT {
    return static_cast<const double &>(
        _solver->parameters.dbase.get<real>("Cp"));
  }

  // get thermal diffusive for testing
  const double &get_thermal_diffusive() const OVCG_NOEXCEPT {
    return static_cast<const double &>(
        _solver->parameters.dbase.get<real>("kThermal"));
  }

  // get density for testing
  const double &get_density(bool test_flag = true) const OVCG_NOEXCEPT {
    if (test_flag)
      return static_cast<const double &>(
          _solver->parameters.dbase.get<real>("fluidDensity"));
    return static_cast<const double &>(
        _solver->parameters.dbase.get<real>("rho"));
  }
};

// define name
const std::string CGins::name = "cgins";

} // namespace ovcg

#endif
