"""This file contains our pattern of CMD file for INS solver"""

cmd_pattern = '''\
# This CMD file was created by OVCG v%s
#
# ESSENTIALS
$grid=%s;
$ovcg_template_root=%s;
# END OF ESSENTIALS
#
# SOLVERS
$solver=%s;         # solver for implicit and pressure
$pc=%s;             # preconditioner
$atol=%.16e;        # abs tolerance
$rtol=%.16e;        # relative tolerance
$ts=%s;             # time advance scheme
# END SOLVERS
#
# PDE/MATARIAL PARAMETERS
$useDimensionalMode=%d;
$kThermal=%.16e;
$nu=%.16e;
$thermalConductivity=%.16e;
$thermalExpansivity=%.16e;
$adcBoussinesq=%.16e;
$useGravity=%d;
$rho=%.16e;
$gravity=\"%.16e %.16e %.16e\";
$Prandtl=%.16e;
$useAtificalDiffusion=%d;       # This is only second order for now
$ad1=%.16e;
$ad2=%.16e;
$useNeumannOutFlow=%d;
# END PDE/MATARIAL PARAMETERS
#
#
if( $solver eq \"best\" ) { $solver=\"choose best iterative solver\"; }
if( $solver eq \"yale\" ) { $solver=\"yale\";                         }
if( $solver eq \"mg\" )   { $solver=\"multigrid\";                    }
if( $ts eq \"fe\" )       { $ts=\"forward Euler\";                    }
if( $ts eq \"be\" ) { \\
    $ts=\"implicit\\nuseNewImplicitMethod\\nimplicit factor 1.0\"; \\
}
if( $ts eq \"mid\" ) { \\
    $ts=\"implicit\\nuseNewImplicitMethod\\nimplicit factor 0.5\"; \\
}
#
# FIRST OF ALL, SPECIFY THE GIRD FILE
#
$grid
#
# PDE MODLE
#
incompressible Navier Stokes
Boussinesq model
    define real parameter kappa $kThermal
    define real parameter thermalExpansivity $thermalExpansivity
    define real parameter adcBoussinesq $adcBoussinesq
continue
#
# WE DON\'T NEED TWILIGHT ZONE!
#
turn off twilight zone
#
# INDICATE OUR ADVANCE SCHEME
#
$ts
#
# CHOOSE OUR PDE PARAMETERS
#
pde parameters
    nu $nu
    kThermal $kThermal
    thermal conductivity $thermalConductivity
    fluid density
        $rho
    if ( $useGravity eq 1 ) { $cmd=$gravity; } else { $cmd=\"0 0 0\"; }
    gravity
        $cmd
#
# INDICATE FOR 2ND ORDER ARTIFICIAL DIFFUSION
#
OBPDE:second-order artificial diffusion $useAtificalDiffusion
OBPDE:ad21,ad22 $ad1, $ad2
#
# FOR OUTFLOW OPTION
#
if ( $useNeumannOutFlow eq 1 ) { $cmd=\"use Neumann BC at outflow\"; } \\
else { $cmd=\"use extrapolate BC at outflow\"; }
$cmd
done
'''
