"""A simple VTK writer for the solver

Ideas borrowed from MESHIO module
"""

from .._version import __version__
import sys
import numpy as np
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
try:
    from StringIo import cStringIO as BytesIO
except ImportError:
    from io import BytesIO
from ..common import filter_out_unused


map2vtk_types = {
    2: 3,  # line
    4: 9,  # quad
    8: 12,  # hex
}

np2vtk_types = {
    np.dtype(np.int32): 'Int32',
    np.dtype(np.int64): 'Int64',
    np.dtype(np.uint32): 'UInt32',
    np.dtype(np.uint64): 'UInt64',
    np.dtype(np.float32): 'Float32',
    np.dtype(np.float64): 'Float64',
}


def numpy_to_xml_array(parent, name, fmt, data):
    da = ET.SubElement(
        parent, 'DataArray',
        type=np2vtk_types[data.dtype],
        Name=name,
    )
    if len(data.shape) == 2:
        da.set('NumberOfComponents', '{}'.format(data.shape[1]))
    da.set('format', 'ascii')
    s = BytesIO()
    np.savetxt(s, data.flatten(), fmt)
    da.text = s.getvalue().decode()


pvd_pattern = '''\
<?xml version=\"1.0\"?>
<VTKFile type=\"Collection\" version=\"0.1\">
  <Collection>
  </Collection>
</VTKFile>
'''

pvd_step_pattern = \
    '''    <DataSet timestep=\"%.16f\" part=\"0\" file="%s"/>\n'''


class VtkWriter(object):
    """A simple vtk writer with Paraview system"""

    @staticmethod
    def create_new():
        vtk_file = ET.Element(
            'VTKFile',
            type='UnstructuredGrid',
            version='0.1',
            byte_order=(
                'LittleEndian' if sys.byteorder == 'little' else 'BigEndian'
            ),
            header_type='UInt32',
            compressor='vtkZLibDataCompressor'
        )
        vtk_file.insert(1, ET.Comment(
            'This file was created by OVCG v{}'.format(__version__)))
        grid = ET.SubElement(vtk_file, 'UnstructuredGrid')
        return vtk_file, grid

    @staticmethod
    def new_piece(parent, points, cells, point_data=None):
        if isinstance(points, list):
            grid_name = points[0]
            coords = points[1]
        else:
            assert isinstance(points, np.ndarray)
            grid_name = 'Points'
            coords = points
        assert isinstance(cells, np.ndarray) and len(cells.shape) == 2
        piece = ET.SubElement(
            parent,
            'Piece',
            NumberOfPoints='{}'.format(coords.shape[0]),
            NumberOfCells='{}'.format(cells.shape[0])
        )
        pts = ET.SubElement(piece, 'Points')
        numpy_to_xml_array(pts, grid_name, '%.16e', coords)
        conn = cells.flatten()
        offsets = cells.shape[1] * \
            np.arange(1, cells.shape[0] + 1, dtype=cells.dtype)
        types = np.full(
            cells.shape[0], map2vtk_types[cells.shape[1]], dtype=cells.dtype)
        c = ET.SubElement(piece, 'Cells')
        numpy_to_xml_array(c, 'connectivity', '%d', conn)
        numpy_to_xml_array(c, 'offsets', '%d', offsets)
        numpy_to_xml_array(c, 'types', '%d', types)
        if point_data:
            assert isinstance(point_data, dict)
            pd = ET.SubElement(piece, 'PointData')
            for n, data in point_data.items():
                numpy_to_xml_array(pd, n, '%.16e', data)

    @staticmethod
    def dump(root, filename):
        assert isinstance(filename, str)
        import xml.dom.minidom as xdm
        rough_string = ET.tostring(root, 'utf-8')
        reparsed = xdm.parseString(rough_string)
        with open(filename, mode='w') as f:
            f.write(reparsed.toprettyxml(indent=2 * ' '))
        f.close()

    @staticmethod
    def create_pvd(filename):
        assert isinstance(filename, str)
        if not filename.endswith('.pvd'):
            filename += '.pvd'
        with open(filename, mode='w') as f:
            f.write(pvd_pattern)
        f.close()

    @staticmethod
    def append_pvd(filename, time_step, vtk_file):
        assert isinstance(vtk_file, str)
        my_pattern = pvd_step_pattern % (time_step, vtk_file)
        f = open(filename, mode='r')
        contents = f.readlines()
        f.close()
        contents.insert(-2, my_pattern)
        contents = ''.join(contents)
        with open(filename, mode='w') as f:
            f.write(contents)
        f.close()


class VtkFile(object):
    """A simple vtk file instance for CG solvers"""

    def __init__(self, filename):
        self._step = 0
        self._file = filename
        if not self._file.endswith('.pvd'):
            self._file += '.pvd'
        base = self._file.split('.')[0]
        base2 = base.split('/')[-1]
        VtkWriter.create_pvd(self._file)
        self._vtu_pattern = base + '%010d.vtu'
        self._vtu_in_pvd = base2 + '%010d.vtu'

    def write(self, solver, time_step):
        root, g = VtkWriter.create_new()
        vtu_file = self._vtu_pattern % self._step
        vtu_in_pvd = self._vtu_in_pvd % self._step
        self._step = self._step + 1
        for grid in range(solver.get_component_grid_number()):
            gname = solver.get_grid_name(grid)
            points_ori = solver.get_coordinates(grid)
            mask = solver.get_mask(grid)
            conn_ori = solver.generate_connectivity(grid)
            slicing, cells = filter_out_unused(mask, conn_ori)
            points = points_ori[slicing]
            T = solver.get_temperature(grid)[slicing]
            V = solver.get_velocity(grid)[slicing]
            P = solver.get_pressure(grid)[slicing]
            if solver.get_dimension() == 2:
                buff = np.zeros(shape=(points.shape[0], 3), dtype=float)
                buff[:, 0:2] = points
                points = buff
                # buff2 = np.zeros(shape=(points.shape[0], 3), dtype=float)
                # buff[:, 0:2] = V
                # V = buff2
            VtkWriter.new_piece(
                g,
                [gname, points],
                cells,
                {
                    'Temperature': T,
                    'Velocity': V,
                    'Pressure': P
                }
            )
        VtkWriter.dump(root, vtu_file)
        VtkWriter.append_pvd(self._file, time_step, vtu_in_pvd)
