import os

_home = os.environ.get('HOME', default=None)
_home = _home if _home else os.environ.get('DOCKER_HOME', default=None)
if _home is None:
    raise EnvironmentError('$HOME or $DOCKER_HOME must be setup!')

_ov_dir = os.environ.get('Overture', default=None)
_ov_dir = _ov_dir if _ov_dir else _home + '/overture/Overture.bin'
_cg_dir = os.environ.get('CGBUILDPREFIX', default=None)
_cg_dir = _cg_dir if _cg_dir else _home + '/overture/cg.bin'
_solvers = ['/ad', '/asf', '/ins', '/cns', '/sm', '/mp']  # NOTE no mx
_cg_dirs = [_cg_dir + sol for sol in _solvers]
