#ifndef _OVCG_DEBUG_HPP
#define _OVCG_DEBUG_HPP 1

#ifndef OVCG_NDEBUG
#include <cstdlib>
#include <iostream>
#define OVCG_ASSERT(__cond, __msg)                                             \
  if (!(__cond)) {                                                             \
    std::cerr << "[OVCG INTERNAL ERROR ALERTED] *Reason*: " << __msg << '\n';  \
    std::exit(1);                                                              \
  }
#else
#define OVCG_ASSERT(__cond, __msg)
#endif

#endif // _OVCG_DEBUG_HPP
