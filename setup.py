import sys
from setuptools import setup
import re
from config import ext

if sys.version_info[:2] < (3, 5):
    print('PyOVCG requires Python 3.5 or higher.')
    sys.exit(-1)

vfile = open('ovcg/_version.py', 'r')
vstr_raw = vfile.read()
vstr_find = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]", vstr_raw, re.M)
if vstr_find:
    version = vstr_find.group(1)
else:
    raise RuntimeError('Unable to find __version__ in ovcg/_version.py.')
vfile.close()

modules = [
    'ovcg',
    'ovcg.common',
    'ovcg.cgmp',
    'ovcg.cgins',
    'ovcg.io',
]

install_req = [
    'numpy >= 1.11',
]

classifiers = [
    'License :: OSI Approved :: BSD License',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3.5',
    'Programming Language :: C++',
    'Topic :: Scientific/Engineering',
    'Topic :: Software Development',
    'Operating System :: Linux',
    'Intended Audience :: Science/Research',
]

long_description = '''\
PyOVCG is a simple Python 3 interface for CG solvers that are developed on top
of Overture framework. The core idea of these numerical PDE solvers is to
utilize the so-called overset grids with finite difference methods. This
approach gives flexiblity on complex geometry as well as the efficiency in
terms of run time performance. Overture and CG are pretty complicated software,
so this project is not aiming at wrapping them completely. Instead, this
software is developed for a specific misson--simulating conjugate heat transfer
problems. PyOVCG is designed to be an mature fluid solver for a Python based
multiphysics coupling framework. We hope this software and its design can be
helpful, however, there are some critical components that are missing in this
software. 1) In general, the solid side is usually discretized by FEM, which
uses unstructured meshes. So structured plus unstructured non matching surface
transfer is missing. 2) For complex geometry models, it's common that we may
have multiple patches of grid on the interface, this arises some interesting
problems: from solid (or non overset side) to overset regions, we treat the
transferring stage independently and assume the overset solvers can handle
the consistency; the opposite way, however, is not straitforward, special
treatments needed to be done, and these involve both methodology and as well
as implementations.\
'''

setup(
    name='ovcg',
    version=version,
    description='Simplified Python Wrapper for Overture/CG',
    author='Qiao Chen',
    author_email='benechiao@gmail.com',
    keywords='Math',
    packages=modules,
    ext_modules=ext,
    install_requires=install_req,
    classifiers=classifiers,
    license='BSD',
    long_description=long_description,
)
