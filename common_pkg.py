_dir = 'ovcg/common/'
_name = 'ovcg.common.'

# index
cy_src0 = _dir + 'indices.pyx'
cpp_src0 = _dir + 'indices.cpp'
name0 = _name + 'indices'
srcs0 = [_dir + 'indices.cpp']

# grid
cy_src1 = _dir + 'grids.pyx'
cpp_src1 = _dir + 'grids.cpp'
name1 = _name + 'grids'
srcs1 = [cpp_src1]

# geometry
cy_src2 = _dir + 'geometry.pyx'
cpp_src2 = _dir + 'geometry.cpp'
name2 = _name + 'geometry'
srcs2 = [cpp_src2]

# functions
cy_src3 = _dir + 'grids_function.pyx'
cpp_src3 = _dir + 'grids_function.cpp'
name3 = _name + 'grids_function'
srcs3 = [cpp_src3]

# utils
cy_src4 = _dir + 'utils.pyx'
cpp_src4 = _dir + 'utils.cpp'
name4 = _name + 'utils'
srcs4 = [cpp_src4]

count = 5
